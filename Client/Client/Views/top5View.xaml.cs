﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Views
{
    public partial class top5View : UserControl
    {
        public string top1 { get; set; } = "No Player";
        public string top2 { get; set; } = "No Player";
        public string top3 { get; set; } = "No Player";
        public string top4 { get; set; } = "No Player";
        public string top5 { get; set; } = "No Player";

        public top5View()
        {
            byte[] code = new byte[1];
            code[0] = 6;
            MainWindow.m_communicator.SendMsg(code);

            Response response = MainWindow.m_communicator.recvMsg();
            GetHighScoreResponse getPersonalStatsResponse = JsonPacketDeserializer.deserializeGetHighScoreResponse(response.data);
            try
            {
                top1 = getPersonalStatsResponse.statistics[0];
                top2 = getPersonalStatsResponse.statistics[1];
                top3 = getPersonalStatsResponse.statistics[2];
                top4 = getPersonalStatsResponse.statistics[3];
                top5 = getPersonalStatsResponse.statistics[4];
            }
            catch (Exception)
            {
            }
            DataContext = this;
            InitializeComponent();
        }
    }
}
