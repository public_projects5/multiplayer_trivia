﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Client.JsonPacketSerializer;
namespace Client.Views
{
    /// <summary>
    /// Interaction logic for CreateRoomView.xaml
    /// </summary>
    public partial class CreateRoomView : UserControl
    {
        public CreateRoomView()
        {
            DataContext = new MainPageViewModel();
            InitializeComponent();
        }

        private void btnSubsit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int maxPlayers = Int32.Parse(txtMaxUsers.Text);
                int questionCount = Int32.Parse(txtQuestionCount.Text);
                int answerTime = Int32.Parse(txtAnswerTimeout.Text);

                if (maxPlayers < 0 || maxPlayers > 15)
                {
                    MessageBox.Show("Max Players must be between 1 to 15!");
                    txtMaxUsers.Clear();
                    return;
                }
                if (questionCount < 1)
                {
                    MessageBox.Show("Question amount must be positive value!");
                    txtQuestionCount.Clear();
                    return;
                }
                if (answerTime < 5 || answerTime > 30)
                {
                    MessageBox.Show("answer timeout count must be between 5 to 30!");
                    txtAnswerTimeout.Clear();
                    return;
                }

                CreateRoomRequest createRoomRequest = new CreateRoomRequest(txtRoomName.Text, int.Parse(txtMaxUsers.Text), int.Parse(txtQuestionCount.Text), int.Parse(txtAnswerTimeout.Text));
                byte[] req = serializeResponse(createRoomRequest);
                MainWindow.m_communicator.SendMsg(req);
                Response response = MainWindow.m_communicator.recvMsg();

                if (response.status != 1)
                {
                    MessageBox.Show(JsonPacketDeserializer.deserializeErrorResponse(response.data).message);
                    txtQuestionCount.Clear();
                }
                else
                {
                    List<roomData> rooms = getRooms();

                    int id = getIdOf(txtRoomName.Text, rooms);

                    for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                        App.Current.Windows[intCounter].Hide();
                    waiting waiting = new waiting(id, 1);
                    waiting.Show();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error! check your input");
                txtRoomName.Clear();
                txtMaxUsers.Clear();
                txtQuestionCount.Clear();
                txtAnswerTimeout.Clear();
            }
          }

        public List<roomData> getRooms()
        {
            List<roomData> rooms;
            byte[] code = new byte[1];
            code[0] = 5;
            MainWindow.m_communicator.SendMsg(code);

            Response response = MainWindow.m_communicator.recvMsg();
            GetRoomsResponse getRoomsResponse = JsonPacketDeserializer.deserializeGetRoomsResponse(response.data);
            rooms = getRoomsResponse.rooms;
            return rooms;
        }

        public int getIdOf(string roomName, List<roomData> rooms)
        {
            for (int i = 0; i < rooms.Count; i++)
            {
                if (rooms[i].name == roomName && rooms[i].isActive == 0)
                    return rooms[i].id;
            }
            return -1;
        }

    }
}
