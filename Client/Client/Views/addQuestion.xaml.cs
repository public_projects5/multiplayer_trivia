﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Client.JsonPacketSerializer;
namespace Client.Views
{
    /// <summary>
    /// Interaction logic for addQuestion.xaml
    /// </summary>
    public partial class addQuestion : UserControl
    {
        public addQuestion()
        {
            InitializeComponent();
        }
        private void btnSubsit_Click(object sender, RoutedEventArgs e)
        {
            if(txtQuestion.Text == "" || txtCorrect.Text == "" || txtWrong1.Text == "" || txtWrong2.Text == "" || txtWrong3.Text == "")
            {
                MessageBox.Show("Error fill all details");
            }
            else
            {
                AddQuestionRequest addQ = new AddQuestionRequest() { question=txtQuestion.Text, correct=txtCorrect.Text, wrong1=txtWrong1.Text, wrong2=txtWrong2.Text, wrong3=txtWrong3.Text };
                byte[] req = serializeResponse(addQ);
                MainWindow.m_communicator.SendMsg(req);
                MainWindow.m_communicator.recvMsg();
            }
        }
    }
}
