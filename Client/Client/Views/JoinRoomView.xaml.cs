﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Client.JsonPacketSerializer;
using static Client.MenuScreen;
namespace Client.Views
{
    /// <summary>
    /// Interaction logic for JoinRoomView.xaml
    /// </summary>
    public partial class JoinRoomView : UserControl
    {
        private BackgroundWorker background_worker = new BackgroundWorker();
        public roomData[] rooms;
        public JoinRoomView()
        {
            InitializeComponent();

            DataContext = new RoomListViewModel();
            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;

            background_worker.DoWork += background_worker_DoWork;
            background_worker.ProgressChanged += background_worker_ProgressChanged;
            background_worker.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync(1000);
        }
        private void btnSubsit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                JoinRoomRequest joinRoomRequest = new JoinRoomRequest(int.Parse(txtRoomID.Text));
                byte[] req = serializeResponse(joinRoomRequest);
                MainWindow.m_communicator.SendMsg(req);
                Response response = MainWindow.m_communicator.recvMsg();
                if (response.status == 1)
                {
                    background_worker.CancelAsync();
                    for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                        App.Current.Windows[intCounter].Hide();
                    waiting waiting = new waiting(int.Parse(txtRoomID.Text), 0);
                    waiting.Show();
                }
                else
                {
                    MessageBox.Show(JsonPacketDeserializer.deserializeErrorResponse(response.data).message);
                    txtRoomID.Clear();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error! check your input");
                txtRoomID.Clear();
            }

        }

        private void background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            byte[] code = new byte[1];
            code[0] = 5;
            
            MainWindow.m_communicator.SendMsg(code);
            Response response = MainWindow.m_communicator.recvMsg();
            GetRoomsResponse getRoomsResponse = JsonPacketDeserializer.deserializeGetRoomsResponse(response.data);
            clearRooms();
            if (getRoomsResponse.rooms != null)
            {
                rooms = getRoomsResponse.rooms.ToArray();
                addRooms();
            }
            InitializeComponent();

        }
        private void addRooms()
        {
            for (int i = 0; i < rooms.Length; i++)
            {
                ListBoxItem newItem = new ListBoxItem();
                newItem.Content = rooms[i].name;
                if (rooms[i].isActive != 1)
                {
                    Name.Items.Add(rooms[i].name);
                    Id.Items.Add(rooms[i].id);
                    Maxplayers.Items.Add(rooms[i].maxPlayers);
                    Numberofquestions.Items.Add(rooms[i].numOfQuestionsInGame);
                    timeperquestion.Items.Add(rooms[i].timePerQuestion);
                    State.Items.Add(rooms[i].isActive);
                }
            }
        }
        private void clearRooms()
        {
            try
            {
                while (Name.Items.Count > 1)
                {

                    Name.Items.RemoveAt(1);
                    Id.Items.RemoveAt(1);
                    Maxplayers.Items.RemoveAt(1);
                    Numberofquestions.Items.RemoveAt(1);
                    timeperquestion.Items.RemoveAt(1);
                    State.Items.RemoveAt(1);
                    Console.WriteLine("clear! --------------- ");

                }
            }
            catch (Exception) { }
        }

        private void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (cancelAsync || background_worker.CancellationPending)   //adding cancelAsync to if statement to close thread when changing window
                {
                    background_worker.CancelAsync();
                    break;
                }
                background_worker.ReportProgress(1, 1);
                Thread.Sleep(3000);
                Console.WriteLine("lap --------------- ");
            }
        }

        private void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //MessageBox.Show("BackgroundWorker canceled");
            }
            else
            {
               // MessageBox.Show("BackgroundWorker ended successfully");
            }
        }
    }
}
