﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace Client.Views
{
    /// <summary>
    /// Interaction logic for RoomListView.xaml
    /// </summary>
    public partial class RoomListView : UserControl
    {
        private int size = 1;
        private BackgroundWorker background_worker = new BackgroundWorker();
        public roomData[] rooms;
        public RoomListView()
        {
            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;

            background_worker.DoWork += background_worker_DoWork;
            background_worker.ProgressChanged += background_worker_ProgressChanged;
            background_worker.RunWorkerAsync(1000);
        }

        private void background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            byte[] code = new byte[1];
            code[0] = 5;

            MainWindow.m_communicator.SendMsg(code);

            Response response = MainWindow.m_communicator.recvMsg();
            GetRoomsResponse getRoomsResponse = JsonPacketDeserializer.deserializeGetRoomsResponse(response.data);


            clearRooms();
            if (getRoomsResponse.rooms != null)
            {
                rooms = getRoomsResponse.rooms.ToArray();
                addRooms();
                size = rooms.Length + 1;
            }
            else
                size = 1;

            InitializeComponent();

        }
        private void addRooms()
        {
            for (int i = 0; i < rooms.Length; i++)
            {
                ListBoxItem newItem = new ListBoxItem();
                newItem.Content = rooms[i].name;
                Name.Items.Add(rooms[i].name);
                Id.Items.Add(rooms[i].id);
                Maxplayers.Items.Add(rooms[i].maxPlayers);
                Numberofquestions.Items.Add(rooms[i].numOfQuestionsInGame);
                timeperquestion.Items.Add(rooms[i].timePerQuestion);
            }
        }
        private void clearRooms()
        {
            try
            {
                for (int i = 1; i < size; i++)
                {

                    Name.Items.RemoveAt(i);
                    Id.Items.RemoveAt(i);
                    Maxplayers.Items.RemoveAt(i);
                    Numberofquestions.Items.RemoveAt(i);
                    timeperquestion.Items.RemoveAt(i);
                    Console.WriteLine("clear! --------------- ");

                }
            }
            catch (Exception) { }
        }

        private void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {

            while (true)
            {
                if (background_worker.CancellationPending)    //if progress stoped we should stop the thread
                {
                    break;
                }
                background_worker.ReportProgress(1, 1);
                Thread.Sleep(3000);
                Console.WriteLine("lap --------------- ");
            }
        }
    }
}
