﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Views
{
    public partial class personalInfoView : UserControl
    {

        public string name { get; set; }
        public string games { get; set; }
        public string correct { get; set; }
        public string time { get; set; }
        public string total { get; set; }

        public personalInfoView()
        {
            byte[] code = new byte[1];
            code[0] = 7;
            MainWindow.m_communicator.SendMsg(code);

            Response response = MainWindow.m_communicator.recvMsg();
            GetPersonalStatsResponse getPersonalStatsResponse = JsonPacketDeserializer.deserializeGetPersonalStatsResponse(response.data);
            name = getPersonalStatsResponse.statistics[0];
            time = getPersonalStatsResponse.statistics[1];
            games = getPersonalStatsResponse.statistics[2];
            correct = getPersonalStatsResponse.statistics[3];
            total = getPersonalStatsResponse.statistics[4];

            DataContext = this;
            InitializeComponent();
        }
    }
}
