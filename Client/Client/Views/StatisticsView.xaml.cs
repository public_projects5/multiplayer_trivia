﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for StatisticsView.xaml
    /// </summary>
    public partial class StatisticsView : UserControl
    {
        public StatisticsView()
        {
            DataContext = new personalInfoViewModel();
            InitializeComponent();
        }

        private void top_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new top5ViewModel();
        }   

        private void info_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new personalInfoViewModel();
        }
    }
}
