﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for waiting.xaml
    /// </summary>
    public partial class waiting : Window
    {
        public int id;
        public int stop = 0;
        public int start = 0;
        private BackgroundWorker background_worker = new BackgroundWorker();    //thread for list of the current players in room
        private List<string> lastPlayersList;
        public GetRoomStateRequest getPlayerInRoom;

        public waiting(int roomId, int admin)
        {
            InitializeComponent();
            id = roomId;
            playersInRoom();
            stateRoom();
            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;

            background_worker.DoWork += background_worker_DoWork;
            background_worker.ProgressChanged += background_worker_ProgressChanged;
            background_worker.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync(1000);

            if (admin == 0)  //member   
            {
                Leave_button.Visibility = Visibility.Visible;
            }
            else    //admin
            {
                Start_button.Visibility = Visibility.Visible;
                Close_button.Visibility = Visibility.Visible;
            }
        }
        private void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (stop != 1 && start != 1)
            {
                Thread.Sleep(3000);     //wait 3 second
                background_worker.ReportProgress(1, 1);
            }
        }

        private void background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            byte[] msg = new byte[1];
            msg[0] = 13;
            Response response = new Response() { };
            getPlayerInRoom.hasGameBegun = 0;

            if (stop != 1 && start != 1)
            {
                Thread.Sleep(1000);
                MainWindow.m_communicator.SendMsg(msg);
                response = MainWindow.m_communicator.recvMsg();
                getPlayerInRoom = JsonPacketDeserializer.deserializeGetRoomStateResponse(response.data);
            }

            if (getPlayerInRoom.hasGameBegun == 1)
            {
                start = 1;
                GameRoom gameRoom = new GameRoom(getPlayerInRoom);
                gameRoom.Show();
                this.Close();
                return;
            }

            if (getPlayerInRoom.hasGameBegun == -1)    //admin closed room
            {
                Leave_button_Click(new object(), new RoutedEventArgs());
                stop = 1;
                return;
            }

            else if (start == 1 || stop == 1 || getPlayerInRoom.hasGameBegun == 1)   //admin started game
            {
                return;
            }

            List<string> playersL = getPlayerInRoom.players;
            if (playersL.Count < lastPlayersList.Count)   //if someone leaved the room
            {
                for (int i = 0; i < ListView.Items.Count; i++)
                {
                    if (playersL.Contains(ListView.Items[i]) == false)
                    {
                        ListView.Items.Remove(ListView.Items[i]);
                    }
                }
            }
            else    //if some one joined or no chaned
            {
                for (int i = 0; i < playersL.Count; i++)
                {
                    ListBoxItem newItem = new ListBoxItem();
                    if (lastPlayersList.Contains(playersL[i]) == false)    //if new user not found in the last list so we should add him
                    {
                        newItem.Content = "player: *" + playersL[i] + "*";
                        ListView.Items.Add(newItem);
                    }
                }
            }
            lastPlayersList = playersL;    //update list
        }

        private void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        private void stateRoom()
        {
            byte[] code = new byte[1];
            code[0] = 13;
            MainWindow.m_communicator.SendMsg(code);
            Response responseState = MainWindow.m_communicator.recvMsg();
            getPlayerInRoom = JsonPacketDeserializer.deserializeGetRoomStateResponse(responseState.data);
            System.Windows.Controls.Label lab = new Label();
            lastPlayersList = getPlayerInRoom.players;
            lab.Content = "Number_of_question: " + getPlayerInRoom.questionCount + " Time_per_question: " + getPlayerInRoom.answerTimeout;
            lab.Foreground = new SolidColorBrush(Colors.Red);
            lab.FontSize = 30;
            lab.Margin = new Thickness(444, 10, 262, 700.4);
            myGrid.Children.Add(lab);

        }

        private void playersInRoom()
        {
            byte[] msg = new byte[6]; //typeCode + len + 1 byte of roomId
            GetPlayerInRoomRequest getPlayerInRoomReq;
            getPlayerInRoomReq.roomid = id;
            msg = JsonPacketSerializer.serializeResponse(getPlayerInRoomReq);
            MainWindow.m_communicator.SendMsg(msg);
            Response response = MainWindow.m_communicator.recvMsg();
            GetPlayersInRoomResponse getPlayerInRoom = JsonPacketDeserializer.deserializeGetPlayersInRoomsResponse(response.data);
            lastPlayersList = getPlayerInRoom.players;

            for (int i = 0; i < lastPlayersList.Count; i++)
            {
                ListBoxItem itm = new ListBoxItem();
                if (i == 0)
                    itm.Content = "admin: *" + lastPlayersList[i] + "*";
                else
                    itm.Content = "player: *" + lastPlayersList[i] + "*";
                ListView.Items.Add(itm);
            }
        }

        private void Leave_button_Click(object sender, RoutedEventArgs e)
        {
            stop = 1;
            background_worker.CancelAsync();
            byte[] code = new byte[1];
            code[0] = 14;
            MainWindow.m_communicator.SendMsg(code);
            Response response = MainWindow.m_communicator.recvMsg();
            MenuScreen menuMember = new MenuScreen();
            menuMember.Show();
            this.Close();
        }

        private void Close_button_Click(object sender, RoutedEventArgs e)
        {
            byte[] code = new byte[1];
            code[0] = 11;
            stop = 1;
            background_worker.CancelAsync();
            MainWindow.m_communicator.SendMsg(code);
            Response response = MainWindow.m_communicator.recvMsg();
            MenuScreen menuAdmin = new MenuScreen();
            menuAdmin.Show();
            this.Close();
        }

        private void Start_button_Click(object sender, RoutedEventArgs e)
        {
            byte[] code = new byte[1];
            code[0] = 12;
            start = 1;
            MainWindow.m_communicator.SendMsg(code);
            Response response = MainWindow.m_communicator.recvMsg();
            background_worker.CancelAsync();
            this.Close();
            GameRoom gameRoom = new GameRoom(getPlayerInRoom);
            gameRoom.Show();

        }
    }
}
