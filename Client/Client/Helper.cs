﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
namespace Client
{
    public static class Helper
    {
        public static int getIntPartFromSocket(NetworkStream sc)
        {
            byte[] buffer = new byte[1];
            int bytesRead = sc.Read(buffer, 0, 1);
            return (int)buffer[0];
        }
        public static int getLenFromSocket(NetworkStream sc)
        {
            string retVal = getPartFromSocket(sc, 4);
            return short.Parse(retVal);
        }
        public static string getStringPartFromSocket(NetworkStream sc, int bytesNum)
        {
            return getPartFromSocket(sc, bytesNum);
        }
        public static string getDataProtocol(string buffer, int typeCode)
        {
            return typeCode.ToString() + getPaddedNumber(buffer.Length, 4) + buffer;
        }
        public static string getPaddedNumber(int num, int digits)
        {
            string os = num.ToString();
            return os.PadLeft(digits,'0');
        }
        private static string getPartFromSocket(NetworkStream sc, int bytesNum)
        {
            byte[] buffer = new byte[bytesNum];
            int bytesRead = sc.Read(buffer, 0, bytesNum);
            return System.Text.Encoding.UTF8.GetString(buffer);
        }
    }
}
