﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;


namespace Client
{
    public class Communicator
    {
        public int m_port;
        public string m_ip;
        public Socket m_socket;
        public NetworkStream m_clientStream;

        public Communicator(int port, string ip)
        {
            m_ip = ip;
            m_port = port;

            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(m_ip), m_port);
            try
            {
                client.Connect(serverEndPoint);
                m_clientStream = client.GetStream();
            }
            catch
            {
                Console.WriteLine("\n\nserver not found!\n\n");
                Environment.Exit(0);
            }
        }


        public int SendMsg(byte[] req)
        {
            m_clientStream.Write(req, 0, req.Length);
            return 0;
        }

        public Response recvMsg()
        {
            int typecode =  Helper.getIntPartFromSocket(m_clientStream);
            int len = Helper.getLenFromSocket(m_clientStream);
            string buffer = Helper.getStringPartFromSocket(m_clientStream, len);
            //return new Response(typecode,Helper.getStringPartFromSocket(m_clientStream, len));
            if (buffer.Contains('*'))
            {
                return new Response() { data = buffer, status = 0 };
            }

            return new Response() { data = buffer, status = 1 };
        }
    }
}
