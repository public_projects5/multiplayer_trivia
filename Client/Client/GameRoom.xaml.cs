﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;
using static Client.JsonPacketSerializer;
using static Client.MenuScreen;
namespace Client
{
    /// <summary>
    /// Interaction logic for GameRoom.xaml
    /// </summary>
    public partial class GameRoom : Window
    {
        public GetRoomStateRequest getPlayerInRoom;
        private System.Windows.Forms.Timer timer1;
        private int counter = 0;
        public int uses = 0;
        public int timeout = 0;
        //public string qustionTXT { get; set; } = "No Question";
        public string answer1 { get; set; } = "No Answer";
        public string answer2 { get; set; } = "No Answer";
        public string answer3 { get; set; } = "No Answer";
        public string answer4 { get; set; } = "No Answer";

        Button color = new Button();

        public GameRoom(GetRoomStateRequest roomStateRequest) //admin- 1, member- 0
        {
            //get users in room:
            InitializeComponent();
            timeout = roomStateRequest.answerTimeout;
            getPlayerInRoom = roomStateRequest;
            color.Background = b1.Background;
            UpdateQustion();
        }

        //function for thread- for room players

        private void UpdateQustion()
        {
            this.Dispatcher.Invoke(() =>
            {
                b1.Background = color.Background;
                b2.Background = color.Background;
                b3.Background = color.Background;
                b4.Background = color.Background;
            });

            byte[] code = new byte[1];
            code[0] = 16;
            MainWindow.m_communicator.SendMsg(code);
            Response response = MainWindow.m_communicator.recvMsg();
            if (response.status != 1)
            {
                //finished to play
                MessageBox.Show(JsonPacketDeserializer.deserializeErrorResponse(response.data).message);
                this.Dispatcher.Invoke(() =>
                {
                    if (uses == 0)
                    {
                        uses = 1;
                        finisedGame(0);
                    }
                });

                return;
            }

            GetQuestionResponse question = JsonPacketDeserializer.deserializeGetQuestionResponse(response.data);

            this.Dispatcher.Invoke(() =>
            {
                qustionTXT.Content = question.question;
                b1.Content = question.answers[4]; // using key (1-4)
                b2.Content = question.answers[3];
                b3.Content = question.answers[2];
                b4.Content = question.answers[1];

                counter = getPlayerInRoom.answerTimeout; // new timer
                timer1 = new System.Windows.Forms.Timer();
                timer1.Tick += new EventHandler(timer1_Tick);
                timer1.Interval = 1000;
                timer1.Start();
                Timer.Content = counter.ToString();
            });
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter--;
            if (counter == 0)
            {
                SubmitAnswerRequest submitAnswerRequest = new SubmitAnswerRequest(5, timeout);
                byte[] req = serializeResponse(submitAnswerRequest);
                MainWindow.m_communicator.SendMsg(req);
                Response response = MainWindow.m_communicator.recvMsg();
                SubmitAnswerResponse correct_answer = JsonPacketDeserializer.deserializeSubmitAnswerResponse(response.data);

                timer1.Stop();
                MessageBox.Show("Out of time!");
                UpdateQustion();
            }
            Timer.Content = counter.ToString();

        }

        private void SubmiteAnswer(int answer, int time) // time - the amount of seconds that passed since the question started 
        {
            SubmitAnswerRequest submitAnswerRequest = new SubmitAnswerRequest(answer, time);
            byte[] req = serializeResponse(submitAnswerRequest);
            MainWindow.m_communicator.SendMsg(req);
            Response response = MainWindow.m_communicator.recvMsg();
            SubmitAnswerResponse correct_answer = JsonPacketDeserializer.deserializeSubmitAnswerResponse(response.data);

            switch (answer) // mark correct answer
            {
                case 1:
                    if (answer == correct_answer.correctAnswer)
                        b1.Background = Brushes.Green;
                    else
                        b1.Background = Brushes.Red;
                    break;
                case 2:
                    if (answer == correct_answer.correctAnswer)
                        b2.Background = Brushes.Green;
                    else
                        b2.Background = Brushes.Red;
                    break;
                case 3:
                    if (answer == correct_answer.correctAnswer)
                        b3.Background = Brushes.Green;
                    else
                        b3.Background = Brushes.Red;
                    break;
                case 4:
                    if (answer == correct_answer.correctAnswer)
                        b4.Background = Brushes.Green;
                    else
                        b4.Background = Brushes.Red;
                    break;
            }
            Task.Delay(1000).ContinueWith(_ =>
            {
                UpdateQustion();
            });
        }

        private void b1_Click(object sender, RoutedEventArgs e)
        {
            timer1.Stop();
            Timer.Content = counter.ToString();
            SubmiteAnswer(1, getPlayerInRoom.answerTimeout - counter);
        }

        private void b2_Click(object sender, RoutedEventArgs e)
        {
            timer1.Stop();
            Timer.Content = counter.ToString();
            SubmiteAnswer(2, getPlayerInRoom.answerTimeout - counter);

        }

        private void b3_Click(object sender, RoutedEventArgs e)
        {
            timer1.Stop();
            Timer.Content = counter.ToString();
            SubmiteAnswer(3, getPlayerInRoom.answerTimeout - counter);
        }

        private void b4_Click(object sender, RoutedEventArgs e)
        {
            timer1.Stop();
            Timer.Content = counter.ToString();
            SubmiteAnswer(4, getPlayerInRoom.answerTimeout - counter);
        }

        private void finisedGame(int status) 
        {
            GetGameResultsResponse GameResults = new GetGameResultsResponse { };
            while (status != 1)
            {
                Thread.Sleep(2000);
                byte[] code = new byte[1];
                code[0] = 18;
                MainWindow.m_communicator.SendMsg(code);
                Response response = MainWindow.m_communicator.recvMsg();
                GameResults = JsonPacketDeserializer.deserializeGetGameResultsResponse(response.data);
                status = response.status;
            }

            EndScreen EndScreen = new EndScreen(GameResults.Results);
            EndScreen.Show();
            this.Close();
        }

        private void Leave_button_Click(object sender, RoutedEventArgs e)
        {
            timer1.Stop();
            byte[] code = new byte[1];
            code[0] = 15;
            MainWindow.m_communicator.SendMsg(code);
            Response response = MainWindow.m_communicator.recvMsg();
            MenuScreen menu = new MenuScreen();
            menu.Show();
            this.Close();
        }
    }
}
