﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Mail;


namespace Client
{
    /// <summary>
    /// Interaction logic for verify.xaml
    /// </summary>
    public partial class verify : Window
    {
        public string randomCode;
        public static string to;

        public verify()
        {
            InitializeComponent();
        }

        private void sendCode_click(object sender, RoutedEventArgs e)
        {
            string from, pass, messageBody;
            Random rand = new Random();
            randomCode = (rand.Next(999999)).ToString();
            MailMessage message = new MailMessage();
            to = (txtEmail.Text).ToString();
            from = "TriviaFun1@gmail.com";
            pass = "trivia123";
            messageBody = "your reset code is: " + randomCode;
            message.To.Add(to);
            message.From = new MailAddress(from);
            message.Body = messageBody;
            message.Subject = "password reseting code";
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.EnableSsl = true;
            smtp.Port = 587;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential(from, pass);
            try
            {
                smtp.Send(message);
                MessageBox.Show("Code send successfully");
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void verify_click(object sender, RoutedEventArgs e)
        {
            if(randomCode == (txtVerCode.Text).ToString())
            {
                to = txtEmail.Text;
                newPassword newPass = new newPassword(txtEmail.Text);
                newPass.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Wrong code");
            }
        }

        private void back_click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }
    }
}
