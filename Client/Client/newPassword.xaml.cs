﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Client.JsonPacketSerializer;

namespace Client
{
    /// <summary>
    /// Interaction logic for newPassword.xaml
    /// </summary>
    public partial class newPassword : Window
    {
        public string m_email;
        public newPassword(string email)
        {
            InitializeComponent();
            m_email = email;
        }

        private void btnSubmit_click(object sender, RoutedEventArgs e)
        { 
            if(txtPassword.ToString() != txtRePassword.ToString())
            {
                MessageBox.Show("Error! passwords are not the same");
                txtPassword.Clear();
                txtRePassword.Clear();
            }
            else
            {
                newPassRequest setPass = new newPassRequest(){ mail=m_email, newPassword = txtPassword.ToString()};
                byte[] req = serializeResponse(setPass);
                MainWindow.m_communicator.SendMsg(req);
                Response response = MainWindow.m_communicator.recvMsg();
                if(response.status != 1)
                {
                    MessageBox.Show(JsonPacketDeserializer.deserializeErrorResponse(response.data).message);
                }
            }
        }

        private void back_click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }
    }
}
