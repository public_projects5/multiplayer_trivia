﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

enum ResponseId
{
	LOGIN_RESPONSE = 1,       
	SIGN_UP_RESPONSE = 2,
	ERROR_RESPONSE = 3,      
	SIGN_OUT_RESPONSE = 4,    
	GET_ROOM_RESPONSE = 5,
	GET_HIGH_SCORE_RESPONSE = 6,
	GET_PERSON_STATS_RESPONSE = 7,
	JOIN_ROOM_RESPONSE = 8,
	CREATE_ROOM_RESPONSE = 9,
	GET_PLAYERS_IN_ROOM = 10,
	CLOSE_ROOM_RESPONSE = 11,
	START_GAME_RESPONSE = 12,
	GET_ROOM_STATE_RESPONSE = 13,
	LEAVE_ROOM_RESPONSE = 14,
	LEAVE_GAME_RESPONSE = 15,
	GET_QUESTION_RESPONSE = 16,
	SUBMIT_ANSWER_RESPONSE = 17,
	GET_GAME_RESULTS_RESPONSE = 18
};

public struct LoginRequest
{
	public string username;
	public string password;

	public LoginRequest(string u, string p)
	{
		username = u;
		password = p;
	}
}

public struct SignupRequest
{
	public string username;
	public string password;
	public string mail;

	public SignupRequest(string u, string p, string m)
	{
		username = u;
		password = p;
		mail = m;

	}
}
public struct JoinRoomRequest
{
	public int roomid;
	public JoinRoomRequest(int id)
	{
		roomid = id;
	}
}

public struct GetPlayerInRoomRequest
{
	public int roomid;
	public GetPlayerInRoomRequest(int id)
	{
		roomid = id;
	}
}

public struct GetRoomIdRequest
{
	public string roonName;
	public GetRoomIdRequest(string name)
	{
		roonName = name;
	}
}

public struct CreateRoomRequest
{
	public string roomName;
	public int maxUsers;
	public int questionCount;
	public int answerTimeout;
	public CreateRoomRequest(string name, int max, int count, int time)
	{
		roomName = name;
		maxUsers = max;
		questionCount = count;
		answerTimeout = time;
	}
}


public struct newPassRequest
{
	public string mail;
	public string newPassword;
}


public struct AddQuestionRequest
{
	public string question;
	public string correct;
	public string wrong1;
	public string wrong2;
	public string wrong3;
}

//Response

public struct Response
{
	public int status;
	public string data;

    public Response(int stat, string info)
    {
		status = stat;
		data = info;
    }
}
public struct LoginResponse
{
	public int status;
}
public struct SignupResponse
{
	public int status;
}
public struct ErrorResponse
{
	public string message;
}
public struct LogoutResponse
{
	public int status;
}
public struct GetRoomsResponse
{
	public int status;
	public List<roomData> rooms;
}
public struct GetPlayersInRoomResponse
{
	public int status;
	public List<string> players;
}

public struct GetHighScoreResponse
{
	public int status;
	public List<string> statistics { get; set; }
}
public struct GetPersonalStatsResponse
{
	public int status;
	public List<string> statistics { get; set; }
}
public struct roomData
{
	public int id { get; set; }
	public string name { get; set; }
	public int maxPlayers { get; set; }
	public int numOfQuestionsInGame { get; set; }
	public int timePerQuestion { get; set; }
	public int isActive { get; set; }
};

public struct GetRoomStateRequest
{
	public int status { get; set; }
	public int hasGameBegun { get; set; }
	public List<string> players { get; set; }
	public int questionCount { get; set; }
	public int answerTimeout { get; set; }
};

public struct GetQuestionResponse
{
	public uint status;
	public string question;
	public Dictionary<uint, string> answers;
}

public struct SubmitAnswerRequest
{
	public int answerId;
	public double answerTime;

	public SubmitAnswerRequest(int answerid, double answertime) : this()
    {
        this.answerId = answerid;
		this.answerTime = answertime;
    }
}
public struct SubmitAnswerResponse
{
	public uint status;
	public uint correctAnswer;
}
public struct PlayerResults
{
    public PlayerResults(string username, int wrongAnswerCount, int correctAnswerCount, double averageAnswerTime)
    {
        this.username = username;
        this.wrongAnswerCount = wrongAnswerCount;
        this.correctAnswerCount = correctAnswerCount;
        this.averageAnswerTime = averageAnswerTime;
    }

    public string username { get; set; }
	public int correctAnswerCount { get; set; }
	public int wrongAnswerCount { get; set; }
	public double averageAnswerTime { get; set; }
}

public struct GetGameResultsResponse
{
	int status;
	public List<PlayerResults> Results { get; set; }
}

public struct AddQuestionResponse
{
	public int status;
}