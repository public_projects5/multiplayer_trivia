﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace Client
{
   
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Communicator m_communicator;
        public MainWindow()
        {
            InitializeComponent();
            m_communicator = new Communicator(8826, "127.0.0.1");
        }

        private void btnLogin(object sender, RoutedEventArgs e)
        {
            LoginScreen login = new LoginScreen();
            login.Show();
            this.Close();
        }

        private void forgotMyPass_click(object sender, RoutedEventArgs e)
        {
            verify ver = new verify();
            ver.Show();
            this.Close();
        }

        private void btnSignup(object sender, RoutedEventArgs e)
        {
            SignupScreen signup = new SignupScreen();
            signup.Show();
            this.Close();
        }
    }
}
