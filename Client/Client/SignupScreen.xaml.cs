﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Client.Communicator;
using static Client.JsonPacketSerializer;

namespace Client
{
    /// <summary>
    /// Interaction logic for SignupScreen.xaml
    /// </summary>
    public partial class SignupScreen : Window
    {
        public SignupScreen()
        {
            InitializeComponent();
        }
        private void btnSubmit_clieck(object sender, RoutedEventArgs e)
        {
            
            int len = txtMail.Text.ToString().Length;
            SignupRequest signup = new SignupRequest(textUsername.Text.ToString(), txtPassword.Password.ToString(), txtMail.Text.ToString());
            byte[] req = serializeResponse(signup);
            MainWindow.m_communicator.SendMsg(req);

            Response response = MainWindow.m_communicator.recvMsg();

            if (response.status == 1)
            {
                MenuScreen menu = new MenuScreen(textUsername.Text.ToString());
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show(JsonPacketDeserializer.deserializeErrorResponse(response.data).message);
                textUsername.Clear();
                txtPassword.Clear();
                txtMail.Clear();
            }
        }
        private void back_click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }
    }
}
