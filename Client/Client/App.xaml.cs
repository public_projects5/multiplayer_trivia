﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Microsoft.Win32;
namespace Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private MediaPlayer mediaPlayer = new MediaPlayer();

        public App()
        {
            String path = ResourceAssembly.Location+"\\..\\..\\..\\Resources\\music.mp3";
            mediaPlayer.Open(new Uri(path, UriKind.Relative));
            mediaPlayer.Play();
        }
    }
}
