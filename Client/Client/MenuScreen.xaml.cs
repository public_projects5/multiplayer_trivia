﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using Client.Views;

namespace Client
{
    /// <summary>
    /// Interaction logic for MenuScreen.xaml
    /// </summary>
    
    public partial class MenuScreen : Window
    {
        public static bool cancelAsync = false;
        public static string userName = "";

        public MenuScreen(string name)
        {
            InitializeComponent();
            userName = name;
            DataContext = new MainPageViewModel();
        }

        public MenuScreen()
        { 
            InitializeComponent();
            DataContext = new MainPageViewModel();
        }

        private void Home_MouseEnter(object sender, MouseEventArgs e)
        {
            popup_uc.PlacementTarget = Home;
            popup_uc.Placement = PlacementMode.Right;
            popup_uc.IsOpen = true;
            Header.PopupText.Text = "Home";
        }

        private void Home_MouseLeave(object sender, MouseEventArgs e)
        {
            popup_uc.Visibility = Visibility.Collapsed;
            popup_uc.IsOpen = false;
        }

        private void CreateRoom_MouseEnter(object sender, MouseEventArgs e)
        {
            popup_uc.PlacementTarget = Create;
            popup_uc.Placement = PlacementMode.Right;
            popup_uc.IsOpen = true;
            Header.PopupText.Text = "Create Room";
        }

        private void CreateRoom_MouseLeave(object sender, MouseEventArgs e)
        {
            popup_uc.Visibility = Visibility.Collapsed;
            popup_uc.IsOpen = false;
        }

        private void JoinRoom_MouseEnter(object sender, MouseEventArgs e)
        {
            popup_uc.PlacementTarget = Join;
            popup_uc.Placement = PlacementMode.Right;
            popup_uc.IsOpen = true;
            Header.PopupText.Text = "Join room";
            
        }

        private void JoinRoom_MouseLeave(object sender, MouseEventArgs e)
        {
            popup_uc.Visibility = Visibility.Collapsed;
            popup_uc.IsOpen = false;
        }

        private void Statistics_MouseEnter(object sender, MouseEventArgs e)
        {
            popup_uc.PlacementTarget = Statistics;
            popup_uc.Placement = PlacementMode.Right;
            popup_uc.IsOpen = true;
            Header.PopupText.Text = "Statistics";
        }

        private void AddQuestion_MouseEnter(object sender, MouseEventArgs e)
        {
            popup_uc.PlacementTarget = AddQuestion;
            popup_uc.Placement = PlacementMode.Right;
            popup_uc.IsOpen = true;
            Header.PopupText.Text = "add question";
        }

        private void Statistics_MouseLeave(object sender, MouseEventArgs e)
        {
            popup_uc.Visibility = Visibility.Collapsed;
            popup_uc.IsOpen = false;
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            //sign out
            byte[] code = new byte[1];
            code[0] = 4;
            MainWindow.m_communicator.SendMsg(code);

            Response response = MainWindow.m_communicator.recvMsg();
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        // changing windows
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            cancelAsync = true; //closing the join room thread
            DataContext = new MainPageViewModel();
        }
        private void Create_Click(object sender, RoutedEventArgs e)
        {
            cancelAsync = true;
            DataContext = new CreateRoomViewModel();
            
        }
        private void Join_Click(object sender, RoutedEventArgs e)
        {
            cancelAsync = false; //opening the join room thread
            DataContext = new JoinRoomViewModel();
        }
        public void Statistic_Click(object sender, RoutedEventArgs e)
        {
            cancelAsync = true;
            DataContext = new StatisticsViewModel();
        }

        private void AddQuestion_Click(object sender, RoutedEventArgs e)
        {
            cancelAsync = true;
            DataContext = new AddQuestionModel();
        }

        private void AddQuestion_MouseLeave(object sender, MouseEventArgs e)
        {
            popup_uc.Visibility = Visibility.Collapsed;
            popup_uc.IsOpen = false;
        }
    }
}
