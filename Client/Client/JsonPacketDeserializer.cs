﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Client
{
    public static class JsonPacketDeserializer
    {
        public static ErrorResponse deserializeErrorResponse(string ErrorResponse)
        {
            return JsonConvert.DeserializeObject<ErrorResponse>(ErrorResponse);
        }
        public static GetPersonalStatsResponse deserializeGetPersonalStatsResponse(string PersonalStatsResponse)
        {
            return JsonConvert.DeserializeObject<GetPersonalStatsResponse>(PersonalStatsResponse);
        }
        public static GetHighScoreResponse deserializeGetHighScoreResponse(string GetHighScoreResponse)
        {
            return JsonConvert.DeserializeObject<GetHighScoreResponse>(GetHighScoreResponse);
        }
        public static GetRoomsResponse deserializeGetRoomsResponse(string GetRoomsResponse)
        {
            return JsonConvert.DeserializeObject<GetRoomsResponse>(GetRoomsResponse);
        }
        public static GetPlayersInRoomResponse deserializeGetPlayersInRoomsResponse(string getPlayerInRoomResponse)
        {
            return JsonConvert.DeserializeObject<GetPlayersInRoomResponse>(getPlayerInRoomResponse);
        }
        public static GetRoomStateRequest deserializeGetRoomStateResponse(string getRoomStateResponse)
        {
            return JsonConvert.DeserializeObject<GetRoomStateRequest>(getRoomStateResponse);
        }
        public static GetQuestionResponse deserializeGetQuestionResponse(string GetQuestionResponse)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(GetQuestionResponse);
            JObject jObject = JObject.Parse(Encoding.UTF8.GetString(bytes));
            GetQuestionResponse questionResponse;
            questionResponse.question = (string)jObject["question"];
            questionResponse.status = (uint)jObject["status"];
            questionResponse.answers = new Dictionary<uint, string>();
            questionResponse.answers.Add(1, (string)jObject["answers"][0][1]);
            questionResponse.answers.Add(2, (string)jObject["answers"][1][1]);
            questionResponse.answers.Add(3, (string)jObject["answers"][2][1]);
            questionResponse.answers.Add(4, (string)jObject["answers"][3][1]);
            return questionResponse;
        }
        public static SubmitAnswerResponse deserializeSubmitAnswerResponse(string SubmitAnswerResponse)
        {
            return JsonConvert.DeserializeObject<SubmitAnswerResponse>(SubmitAnswerResponse);
        }
        public static GetGameResultsResponse deserializeGetGameResultsResponse(string GetGameResultsResponse)
        {
            return JsonConvert.DeserializeObject<GetGameResultsResponse>(GetGameResultsResponse);
        }
    }
    
}
