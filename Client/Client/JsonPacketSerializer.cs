﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using static Client.Helper;

namespace Client
{
    public static class JsonPacketSerializer
    {
        public static byte[] msgAsBinary(byte[] bufferProtocol, string lenStr, string json, int typeCode)
        {
            bufferProtocol[0] = (byte)(typeCode);// + 48);
            for (int i = 0; i < 4; i++)
                bufferProtocol[1 + i] = (byte)lenStr[i];

            for (int i = 0; i < json.Length; i++)
                bufferProtocol[5 + i] = (byte)json.ToCharArray()[i];
            return bufferProtocol;
        }

        public static byte[] serializeResponse(LoginRequest loginReq)
        {
            string json = JsonConvert.SerializeObject(loginReq, Formatting.Indented);
            string lenStr = getPaddedNumber(json.Length, 4);
            byte[] bufferProtocol = new byte[5 + json.Length];
            bufferProtocol = msgAsBinary(bufferProtocol, lenStr, json, 1);
            return bufferProtocol;
        }

        public static byte[] serializeResponse(SignupRequest signupReq)
        {
            string json = JsonConvert.SerializeObject(signupReq, Formatting.Indented);
            string lenStr = getPaddedNumber(json.Length, 4);
            byte[] bufferProtocol = new byte[5 + json.Length];
            bufferProtocol = msgAsBinary(bufferProtocol, lenStr, json, 2);
            return bufferProtocol;
        }

        public static byte[] serializeResponse(JoinRoomRequest joinRoomReq)
        {
            string json = JsonConvert.SerializeObject(joinRoomReq, Formatting.Indented);
            string lenStr = getPaddedNumber(json.Length, 4);
            byte[] bufferProtocol = new byte[5 + json.Length];
            bufferProtocol = msgAsBinary(bufferProtocol, lenStr, json, 8);
            return bufferProtocol;
        }

        public static byte[] serializeResponse(CreateRoomRequest createRoomReq)
        {
            string json = JsonConvert.SerializeObject(createRoomReq, Formatting.Indented);
            string lenStr = getPaddedNumber(json.Length, 4);
            byte[] bufferProtocol = new byte[5 + json.Length];
            bufferProtocol = msgAsBinary(bufferProtocol, lenStr, json, 9);
            return bufferProtocol;
        }

        public static byte[] serializeResponse(GetPlayerInRoomRequest getPlayerInRoomReq)
        {
            string json = JsonConvert.SerializeObject(getPlayerInRoomReq, Formatting.Indented);
            string lenStr = getPaddedNumber(json.Length, 4);
            byte[] bufferProtocol = new byte[5 + json.Length];
            bufferProtocol = msgAsBinary(bufferProtocol, lenStr, json, 10);
            return bufferProtocol;
        }

        public static byte[] serializeResponse(GetRoomIdRequest getRoom)
        {
            string json = JsonConvert.SerializeObject(getRoom, Formatting.Indented);
            string lenStr = getPaddedNumber(json.Length, 4);
            byte[] bufferProtocol = new byte[5 + json.Length];
            bufferProtocol = msgAsBinary(bufferProtocol, lenStr, json, 99);
            return bufferProtocol;
        }
        public static byte[] serializeResponse(SubmitAnswerRequest AnswerRequest)
        {
            string json = JsonConvert.SerializeObject(AnswerRequest, Formatting.Indented);
            string lenStr = getPaddedNumber(json.Length, 4);
            byte[] bufferProtocol = new byte[5 + json.Length];
            bufferProtocol = msgAsBinary(bufferProtocol, lenStr, json, 17);
            return bufferProtocol;
        }

        public static byte[] serializeResponse(newPassRequest newPassReq)
        {
            string json = JsonConvert.SerializeObject(newPassReq, Formatting.Indented);
            string lenStr = getPaddedNumber(json.Length, 4);
            byte[] bufferProtocol = new byte[5 + json.Length];
            bufferProtocol = msgAsBinary(bufferProtocol, lenStr, json, 111);
            return bufferProtocol;
        }

        public static byte[] serializeResponse(AddQuestionRequest addQuestionReq)
        {
            string json = JsonConvert.SerializeObject(addQuestionReq, Formatting.Indented);
            string lenStr = getPaddedNumber(json.Length, 4);
            byte[] bufferProtocol = new byte[5 + json.Length];
            bufferProtocol = msgAsBinary(bufferProtocol, lenStr, json, 66);
            return bufferProtocol;
        }
    }
}
