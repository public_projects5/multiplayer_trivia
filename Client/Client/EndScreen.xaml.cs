﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for EndScreen.xaml
    /// </summary>
    public partial class EndScreen : Window
    {
        public EndScreen(List<PlayerResults> Results)
        {
            InitializeComponent();

            for (int i = 0; i < Results.Count; i++)
            {
                ListBoxItem newItem = new ListBoxItem();
                newItem.Content = Results[i].username;
                Name.Items.Add(Results[i].username);
                Corrects.Items.Add(Results[i].correctAnswerCount);
                Wrongs.Items.Add(Results[i].wrongAnswerCount);
                Time.Items.Add(Results[i].averageAnswerTime);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            byte[] code = new byte[1];
            code[0] = 15;
            MainWindow.m_communicator.SendMsg(code);
            Response response = MainWindow.m_communicator.recvMsg();
            MenuScreen menuMember = new MenuScreen();
            menuMember.Show();
            this.Close();
        }
    }
}
