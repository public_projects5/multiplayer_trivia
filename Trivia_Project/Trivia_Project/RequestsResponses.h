#pragma once
#include <ctime>
#include <vector>
#include <string>
#include <map>
#include "IRequestHandler.h"
#include "Question.h"

class IRequestHandler;

/*
others:
*/

#define ERROR "ERROR"

enum responseId {
	LOGIN_RESPONSE = 1,       //loginHandler
	SIGN_UP_RESPONSE = 2,
	NEW_PASSWORD = 111,
	ERROR_RESPONSE = 3,      //error
	SIGN_OUT_RESPONSE = 4,    //menuHandler
	GET_ROOM_RESPONSE = 5,
	GET_HIGH_SCORE_RESPONSE = 6,
	GET_PERSON_STATS_RESPONSE = 7,
	JOIN_ROOM_RESPONSE = 8,
	CREATE_ROOM_RESPONSE = 9,
	GET_PLAYERS_IN_ROOM = 10,
	CLOSE_ROOM_RESPONSE = 11,
	ADD_QUESTION = 66,
	START_GAME_RESPONSE = 12,
	GET_ROOM_STATE_RESPONSE = 13,
	LEAVE_ROOM_RESPONSE = 14,
	LEAVE_GAME_RESPONSE = 15,
	GET_QUESTION_RESPONSE = 16,
	SUBMIT_ANSWER_RESPONSE = 17,
	GET_GAME_RESULTS_RESPONSE = 18
};

struct roomData {
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	int isActive;
};

struct PlayerResults
{
	std::string username;
	int correctAnswerCount;
	int wrongAnswerCount;
	double averageAnswerTime;
};

struct	GameData
{
	Question *currentQuestion;
	int currentNumQuestion;
	int correctAnswerCount;
	int wrongAnswerCount;
	double averageAnswerTime;
};

enum errors {
	SUCCESS = 1,
	FAIL = 0
};


struct UpdateStateTable
{
	std::string username;
	float averageAnswerTime;
	unsigned int games;
	unsigned int correctAnswers;
	unsigned int totalAnswers;

};

/*
Requests:
*/

struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

struct RequestInfo
{
	int id;
	char* receivalTime;
	char* buffer;
};

struct RequestResult
{
	char* buffer;
	IRequestHandler* newHandler;
};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct SubmitAnswerRequest
{
	unsigned int answerId;
	unsigned int answerTime;
};

struct NewPasswordRequest
{
	std::string email;
	std::string newPassword;
};

struct AddQuestionRequest
{
	std::string question;
	std::string correct;
	std::string wrong1;
	std::string wrong2;
	std::string wrong3;
};


/*
Responses:
*/


struct LoginResponse
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

struct ErrorResponse
{
	std::string message = ERROR;
};

struct LogoutResponse
{
	unsigned int status;
};

struct GetRoomsResponse
{
	unsigned int status;
	std::vector<roomData> rooms;
};

struct GetPlayersInRoomResponse
{
	unsigned int status;
	std::vector<std::string> players;
};

struct GetHighScoreResponse
{
	unsigned int status;
	std::vector<std::string> statistics;
};

struct GetPersonalStatsResponse
{
	unsigned int status;
	std::vector<std::string> statistics;
};

struct JoinRoomResponse
{
	unsigned int status;
};

struct CreateRoomResponse
{
	unsigned int status;
};

struct ClostRoomResponse
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};

struct GetRoomStatsResponse
{
	unsigned int status;
	int hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	int answerTimeOut;
};

struct LeaveRoomResponse
{
	unsigned int status;
};

struct LeaveGameResponse
{
	unsigned int status;
};

struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::map<unsigned int, std::string> answers;
};


struct SubmitAnswerResponse
{
	unsigned int status;
	int correctAnswer;
};

struct GetGameResultsResponse
{
	unsigned int status;
	std::vector<PlayerResults> results;
};

struct NewPasswordResponse {
	unsigned int status;
};

struct AddQuestionResponse {
	unsigned int stauts;
};