#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(RequestHandlerFactory* handlerFanctory, Game* game, LoggedUser* loggedUser, GameManager* gameManager)
{
	m_game = game;
	m_loggedUser = loggedUser;
	m_gameManager = gameManager;
	m_handlerFanctory = handlerFanctory;
}

bool GameRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return requestInfo.id == LEAVE_GAME_RESPONSE || requestInfo.id == GET_QUESTION_RESPONSE ||
		requestInfo.id == SUBMIT_ANSWER_RESPONSE || requestInfo.id == GET_GAME_RESULTS_RESPONSE;
}

RequestResult GameRequestHandler::handlerRequest(RequestInfo requestInfo)
{
    RequestResult reqResult;
    if (!isRequestRelevant(requestInfo))     //return empty result
    {
        char* empty = new char();
        return *new RequestResult{ empty, this };    //enmpy struct
    }
    switch (requestInfo.id)
    {
    case LEAVE_GAME_RESPONSE:
        reqResult = leave(requestInfo);
        break;
    case GET_QUESTION_RESPONSE:
        reqResult = getQuestions(requestInfo);
        break;
    case SUBMIT_ANSWER_RESPONSE:
        reqResult = submitAnswer(requestInfo);
        break;
    case GET_GAME_RESULTS_RESPONSE:
        reqResult = getResults(requestInfo);
        break;
    default:
        break;
    }
    return reqResult;
}

RequestResult GameRequestHandler::leave(RequestInfo requestInfo)
{
    RequestResult reqResult{};
    LeaveGameResponse leaveGameRes;
    m_game->removePlayer(*m_loggedUser);
    reqResult.newHandler = m_handlerFanctory->createMenuRequestHandler(m_loggedUser->getUserName());
    leaveGameRes.status = SUCCESS;
    std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(leaveGameRes);
    helper::copyBuffer(&reqResult.buffer, jsonResponse);

    return reqResult;
}

RequestResult GameRequestHandler::getQuestions(RequestInfo requestInfo)
{
    RequestResult reqResult{};
    GetQuestionResponse getQuestionsRes;
    roomData roomData;
    std::string jsonResponse;
    getQuestionsRes.status = SUCCESS;
    Question question = m_game->getQuestionForUser(*m_loggedUser);

    if (question.getCorrectAnswer() == "") {    //empty
        ErrorResponse errorRes;
        m_game->removePlayer(*m_loggedUser);
        errorRes.message = "*all questions was answered!";
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(errorRes);
        helper::copyBuffer(&reqResult.buffer, jsonResponse);
        reqResult.newHandler = this;
        return reqResult;
    }

    getQuestionsRes.question = question.getQuestion();
    std::map<unsigned int, std::string> *answers = new std::map<unsigned int, std::string>();
    std::vector<std::string> possibleAns = question.getPossibleAnswer();

    //insert answers by random order
    for (int i = 0; i < 4; i++) {
        getQuestionsRes.answers.insert({ i+1, possibleAns[i] });
    }

    jsonResponse = JsonResponsePacketSerializer::serializeResponse(getQuestionsRes);
    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    reqResult.newHandler = this;
    return reqResult;
}

RequestResult GameRequestHandler::submitAnswer(RequestInfo requestInfo)
{
    RequestResult reqResult;
    reqResult.buffer = nullptr;
    std::string jsonResponse = "";
    SubmitAnswerRequest submit = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(requestInfo.buffer);
    SubmitAnswerResponse submitRes;

    Question q = m_game->getQuestionForUser(*m_loggedUser);
    int x = q.getCorrectAnswerNum();
    submitRes.correctAnswer = x;
    m_game->submitAnswer(*m_loggedUser, submit.answerId, submit.answerTime);
    
    submitRes.status = SUCCESS; 
    jsonResponse = JsonResponsePacketSerializer::serializeResponse(submitRes);

    reqResult.newHandler = this;
    helper::copyBuffer(&reqResult.buffer, jsonResponse);

    return reqResult;
}

RequestResult GameRequestHandler::getResults(RequestInfo requestInfo)
{
    RequestResult reqResult;
    GetGameResultsResponse getGameResultsRes;
    std::string jsonResponse = "";
    
    getGameResultsRes.results = m_game->getResults();

    if (getGameResultsRes.results.size() == 0)   //game still running
    {
        ErrorResponse errorRes;
        errorRes.message = "*You have finished playing, wait for the other participants to finish as well";
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(errorRes);
        helper::copyBuffer(&reqResult.buffer, jsonResponse);
        reqResult.newHandler = this;
        return reqResult;
    }

    getGameResultsRes.status = SUCCESS;
    //updata the database
    if (getGameResultsRes.results.size() > 0)   //the game ended
    {
        for (std::vector<PlayerResults>::iterator it = getGameResultsRes.results.begin(); it != getGameResultsRes.results.end(); it++) {
            if (it->username == m_loggedUser->getUserName()) {
                m_gameManager->updatePlayerResults(m_loggedUser, *it);
            }
        }
    }

    jsonResponse = JsonResponsePacketSerializer::serializeResponse(getGameResultsRes);
    reqResult.newHandler = this;
    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    return reqResult;
}
