#pragma once
#include <map>
#include "Room.h"

class RoomManager
{
public:
	RoomManager();
	Room* createRoom(LoggedUser loggenUser, roomData roomD);
	void deleteRoom(unsigned int ID);
	unsigned int getRoomState(unsigned int ID);
	std::vector<roomData> getRooms();
	Room* getRoom(unsigned int roomID);
	void clearRooms();

private:
	std::map<unsigned int, Room>* m_rooms;
};