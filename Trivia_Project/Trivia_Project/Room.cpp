#include "Room.h"
#include <time.h>
Room::Room(std::string name, unsigned int maxPlayers, unsigned int numOfQuestionsInGame, unsigned int timePerQuestion)
{
	srand(time(NULL));
	m_metadata.id = rand() % 10000 + 100;;
	m_metadata.name = name;
	m_metadata.maxPlayers = maxPlayers;
	m_metadata.numOfQuestionsInGame = numOfQuestionsInGame;
	m_metadata.timePerQuestion = timePerQuestion;
	m_metadata.isActive = 0;
	m_users = new std::vector<LoggedUser>();
}

Room::Room(Room* roomOther)
{
	m_metadata = roomOther->getRoom();
	m_users = roomOther->getUsers();
}

void Room::addUser(LoggedUser loggedUser)
{
	m_users->push_back(loggedUser);
}

void Room::removeUser(LoggedUser loggedUser)
{
	for (std::vector<LoggedUser>::iterator it = m_users->begin(); it != m_users->end(); it++) {
		if (static_cast<LoggedUser>(*it).getUserName() == loggedUser.getUserName()) {
			it = m_users->erase(it);
			break;
		}
	}
}

std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> users;

	for (std::vector<LoggedUser>::iterator it = m_users->begin(); it != m_users->end(); it++) {
		users.push_back(static_cast<LoggedUser>(*it).getUserName());

	}
	return users;
}

std::vector<LoggedUser>* Room::getUsers()
{
	return m_users;
}

roomData Room::getRoom() const
{
	return m_metadata;
}

int Room::getId()
{
	return m_metadata.id;
}

void Room::setActivity(int activity)
{
	m_metadata.isActive = activity;
}
