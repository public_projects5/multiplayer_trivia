#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const char* login)
{
    std::string loginStr = std::string(login);
    json loginJson = json::parse(loginStr);
    LoginRequest loginReq;
    loginReq.username = loginJson["username"];
    loginReq.password = loginJson["password"];
    return loginReq;
    //json response: {"username":<username>,"password":<password>}
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const char* signup)
{
    std::string signJsonStr = std::string(signup);
    json signupJson = json::parse(signJsonStr);
    SignupRequest signupReq;
    signupReq.username = signupJson["username"];
    signupReq.password = signupJson["password"];
    signupReq.email = signupJson["mail"];
    return signupReq;
    //json response: {"username":<username>,"password":<password>,"email":<email>}
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayerRequest(const char* getPlayer)
{
    std::string getPlayerStr = std::string(getPlayer);
    json reqJson = json::parse(getPlayerStr);
    GetPlayersInRoomRequest getPlayerReq;
    getPlayerReq.roomId = reqJson["roomid"];
    return getPlayerReq;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const char* joinRoom)
{
    std::string joinRoomStr = std::string(joinRoom);
    json reqJson = json::parse(joinRoomStr);
    JoinRoomRequest joinRoomReq;
    joinRoomReq.roomId = reqJson["roomid"];
    return joinRoomReq;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(const char* createRoom)
{
    std::string createRoomStr = std::string(createRoom);
    json reqJson = json::parse(createRoomStr);
    CreateRoomRequest createRoomReq;
    createRoomReq.roomName = reqJson["roomName"];
    createRoomReq.maxUsers = reqJson["maxUsers"];
    createRoomReq.questionCount = reqJson["questionCount"];
    createRoomReq.answerTimeout = reqJson["answerTimeout"];
    return createRoomReq;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(const char* submitAnswer)
{
    std::string submitAnserStr = std::string(submitAnswer);
    json reqJson = json::parse(submitAnserStr);
    SubmitAnswerRequest submitAnswerReq;
    submitAnswerReq.answerId = reqJson["answerId"];
    submitAnswerReq.answerTime = reqJson["answerTime"];
    return submitAnswerReq;
}

NewPasswordRequest JsonRequestPacketDeserializer::deserializeNewPasswordRequest(const char* newPass)
{
    std::string newPassStr = std::string(newPass);
    json reqJson = json::parse(newPassStr);
    NewPasswordRequest newPassReq;
    newPassReq.email = reqJson["email"];
    newPassReq.newPassword = reqJson["newPassword"];
    return newPassReq;
}

AddQuestionRequest JsonRequestPacketDeserializer::deserializeAddQuestion(const char* addQuestion)
{
    std::string newPassStr = std::string(addQuestion);
    json reqJson = json::parse(newPassStr);
    AddQuestionRequest addQReq;
    addQReq.question = reqJson["question"];
    addQReq.correct = reqJson["correct"];
    addQReq.wrong1 = reqJson["wrong1"];
    addQReq.wrong2 = reqJson["wrong2"];
    addQReq.wrong3 = reqJson["wrong3"];
    return addQReq;
}
