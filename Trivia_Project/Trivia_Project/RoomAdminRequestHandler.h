#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RequestHandlerFactory.h"
#include "RequestsResponses.h"


class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(RequestHandlerFactory* requestHandlerFactory, std::string userName, RoomManager* roomMnanager, Room* room);
	~RoomAdminRequestHandler();
	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult handlerRequest(RequestInfo requestInfo);

private:
	Room* m_room;
	LoggedUser* m_loggedUser;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handlerFanctory;
	RequestResult startGame(RequestInfo reqInfo);
	RequestResult closeRoom(RequestInfo reqInfo);
	RequestResult getRoomStatse(RequestInfo reqInfo);
};

