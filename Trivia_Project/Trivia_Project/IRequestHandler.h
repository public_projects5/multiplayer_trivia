#pragma once
#include <ctime>
#include <vector>
#include "helper.h"

struct RequestResult;
struct RequestInfo;


class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo requestInfo) = 0;
	virtual RequestResult handlerRequest(RequestInfo requestInfo) = 0;
};
