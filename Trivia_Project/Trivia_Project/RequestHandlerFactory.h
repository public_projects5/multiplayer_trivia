#pragma once
#include "LoginRequestHandler.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "MenuRequestHandler.h"
#include "RoomMemeberRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "GameRequestHandler.h"
#include "GameManager.h"

class MenuRequestHandler;
class LoginRequestHandler;
class RoomAdminRequestHandler;
class RoomMemeberRequestHandler;
class GameRequestHandler;


class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDatabase* db, StatisticsManager* statisticManager, LoginManager* loginManager, RoomManager* roomManager, GameManager* gameManager);
	~RequestHandlerFactory();
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager* getLoginManager();
	RoomManager& getRoomManager();
	MenuRequestHandler* createMenuRequestHandler(std::string username);
	StatisticsManager& getStatisticManager();
	RoomAdminRequestHandler* createRoomAdminRequestHandler(Room* room, std::string username);
	RoomMemeberRequestHandler* createRoomMemberRequestHandler(Room* room, std::string username);
	GameRequestHandler* createGameRequestHandler(LoggedUser *loggedUser, Room* room, int premission);
	GameManager& getGameManager();
	IDatabase* getDateBase();

	std::vector<int>* getVector();
	void printVector(std::vector<int>* toPrint) const;
	
private:
	LoginManager* m_loginManager;
	IDatabase* m_database;
	RoomManager* m_roomManager;
	StatisticsManager* m_statisticManager;
	GameManager* m_gameManager;

	std::vector<int>* m_counter;

};