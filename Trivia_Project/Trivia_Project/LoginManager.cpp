#include "LoginManager.h"


LoginManager::LoginManager(IDatabase* database) : m_database(database)
{
	m_loggedUsers = new vector<LoggedUser>();
	m_loggedUsers->push_back(*new LoggedUser("none"));
}

LoginManager::~LoginManager()
{
	delete m_loggedUsers;
}

int LoginManager::signup(string name, string password, string email)
{
	if (!m_database->doesUserExist(name)) {
		m_database->addNewUser(name, password, email);
		m_loggedUsers->push_back(LoggedUser(name));
		return 1;
	}
	return 0;
}

int LoginManager::login(string name, string password)
{
	if (m_database->doesPasswordMatch(name, password)) {
		m_loggedUsers->push_back(LoggedUser(name));
		return 1;
	}
	return 0;
}

void LoginManager::logout(string name)
{
	auto it = m_loggedUsers->begin();
	while (it != m_loggedUsers->end())
	{
		if (it->getUserName() == name)
		{
			m_loggedUsers->erase(it);
			return;
		}
		it++;
	}
}

boolean LoginManager::inPlayerActive(std::string username)
{
	for (int i = 0; i < m_loggedUsers->size(); i++) {
		if (username == (m_loggedUsers->begin() + i)->getUserName())
			return true;
	}
	return false;
}

int LoginManager::newPassword(std::string newPass, std::string email)
{
	return m_database->changePassword(newPass, email);
}
