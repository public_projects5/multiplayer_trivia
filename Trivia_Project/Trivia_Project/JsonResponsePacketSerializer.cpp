#include "JsonResponsePacketSerializer.h"
#include "helper.h"

std::string JsonResponsePacketSerializer::serializeResponse(LoginResponse login)
{
	json bufferData;
	bufferData["status"] = login.status;
	std::string jsonStr = bufferData.dump();
	std::string buffer = helper::getDataProtocol(jsonStr, LOGIN_RESPONSE);
	return buffer.c_str();
}

std::string JsonResponsePacketSerializer::serializeResponse(SignupResponse signUp)
{
	json bufferData;
	bufferData["status"] = signUp.status;
	std::string jsonStr = bufferData.dump();
	std::string buffer = helper::getDataProtocol(jsonStr, SIGN_UP_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(ErrorResponse error)
{
	json bufferData;
	bufferData["message"] = error.message;
	std::string jsonStr = bufferData.dump();
	std::string buffer = helper::getDataProtocol(jsonStr, ERROR_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(LogoutResponse logoutRes)
{
	json bufferData;
	bufferData["status"] = logoutRes.status;
	std::string jsonStr = bufferData.dump();
	std::string buffer = helper::getDataProtocol(jsonStr, SIGN_OUT_RESPONSE);
	return buffer;
}


std::string JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse getRoomRes)
{
	json bufferData;
	json room;
	json allRooms;
	for (std::vector<roomData>::iterator it = getRoomRes.rooms.begin(); it != getRoomRes.rooms.end(); it++) {
		room["name"] = it->name;
		room["maxPlayers"] = it->maxPlayers;
		room["numOfQuestionsInGame"] = it->numOfQuestionsInGame;
		room["timePerQuestion"] = it->timePerQuestion;
		room["isActive"] = it->isActive;
		room["id"] = it->id;
		allRooms.push_back(room);
	}
	bufferData["status"] = getRoomRes.status;
	bufferData["rooms"] = allRooms;
	std::string jsonStr = bufferData.dump();
	std::string buffer = helper::getDataProtocol(jsonStr, GET_ROOM_RESPONSE);
	return buffer;
}

char* JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse getPlayersInRoomRes)
{
	json bufferData;

	bufferData["status"] = getPlayersInRoomRes.status;
	bufferData["players"] = getPlayersInRoomRes.players;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, GET_PLAYERS_IN_ROOM);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse joinRoomRes)
{
	json bufferData;
	bufferData["status"] = joinRoomRes.status;
	std::string jsonStr = bufferData.dump();
	std::string buffer = helper::getDataProtocol(jsonStr, JOIN_ROOM_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse createRoomRes)
{
	json bufferData;
	bufferData["status"] = createRoomRes.status;
	std::string jsonStr = bufferData.dump();
	std::string buffer = helper::getDataProtocol(jsonStr, CREATE_ROOM_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(GetPersonalStatsResponse getPersonalStatsRes)
{
	json bufferData;
	bufferData["status"] = getPersonalStatsRes.status;
	bufferData["statistics"] = getPersonalStatsRes.statistics;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, GET_PERSON_STATS_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse getHighScoreRes)
{
	json bufferData;
	bufferData["status"] = getHighScoreRes.status;
	bufferData["statistics"] = getHighScoreRes.statistics;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, GET_HIGH_SCORE_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(ClostRoomResponse clostRoomRes)
{
	json bufferData;
	bufferData["status"] = clostRoomRes.status;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, JOIN_ROOM_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(StartGameResponse startGameRes)
{
	json bufferData;
	bufferData["status"] = startGameRes.status;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, JOIN_ROOM_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(GetRoomStatsResponse getRoomStatsRes)
{
	json bufferData;
	bufferData["status"] = getRoomStatsRes.status;
	bufferData["hasGameBegun"] = getRoomStatsRes.hasGameBegun;
	bufferData["players"] = getRoomStatsRes.players;
	bufferData["questionCount"] = getRoomStatsRes.questionCount;
	bufferData["answerTimeOut"] = getRoomStatsRes.answerTimeOut;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, JOIN_ROOM_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse leaveRoomRes)
{
	json bufferData;
	bufferData["status"] = leaveRoomRes.status;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, JOIN_ROOM_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse getGameResultsRes)
{
	json bufferData;
	json result;
	json allResults;
	
	for (std::vector<PlayerResults>::iterator it = getGameResultsRes.results.begin(); it != getGameResultsRes.results.end(); it++) {
		result["username"] = it->username;
		result["correctAnswerCount"] = it->correctAnswerCount;
		result["wrongAnswerCount"] = it->wrongAnswerCount;
		result["averageAnswerTime"] = it->averageAnswerTime;
		allResults.push_back(result);
	}

	bufferData["status"] = getGameResultsRes.status;
	bufferData["results"] = allResults;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, JOIN_ROOM_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse submitAnswerRes)
{
	json bufferData;
	bufferData["status"] = submitAnswerRes.status;
	bufferData["correctAnswer"] = submitAnswerRes.correctAnswer;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, JOIN_ROOM_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse getQuestionRes)
{
	json bufferData;
	bufferData["status"] = getQuestionRes.status;
	bufferData["question"] = getQuestionRes.question;
	bufferData["answers"] = getQuestionRes.answers;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, JOIN_ROOM_RESPONSE);
	return buffer;	
}

std::string JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse leaveGameRes)
{
	json bufferData;
	bufferData["status"] = leaveGameRes.status;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, JOIN_ROOM_RESPONSE);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(NewPasswordResponse newPassRes)
{
	json bufferData;
	bufferData["status"] = newPassRes.status;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, NEW_PASSWORD);
	return buffer;
}

std::string JsonResponsePacketSerializer::serializeResponse(AddQuestionResponse addQuestionRes)
{
	json bufferData;
	bufferData["status"] = addQuestionRes.stauts;
	std::string jsonStr = bufferData.dump();
	char* buffer = helper::getDataProtocol(jsonStr, NEW_PASSWORD);
	return buffer;
}
