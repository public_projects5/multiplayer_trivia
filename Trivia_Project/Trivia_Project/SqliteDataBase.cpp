#include "SqliteDataBase.h"
bool result = false;
string callback_data;
static int callback_doesUserExist(void* data, int argc, char** argv, char** azColName)
{
	result = true;
	return 0;
}
static int callback_qestions(void* data, int argc, char** argv, char** azColName)
{
	//Question(std::string question, std::string correctAns, std::string wrong1, std::string wrong2, std::string wrong3);
	std::vector<Question*>* questions = reinterpret_cast<std::vector<Question*>*>(data);
	Question* q = new Question(argv[1], argv[2], argv[3], argv[4], argv[5]);
	questions->push_back(q);
	return 0;
}

static int callback_USERS(void* data, int argc, char** argv, char** azColName)
{
	list<string>* users = reinterpret_cast<list<string>*>(data);
	for (int i = 0; i < argc; i++)
	{
		users->push_back(argv[i]);
	}
	return 0;
}
static int callback_stats(void* data, int argc, char** argv, char** azColName)
{
	float* my_data = static_cast<float*>(data);
	int index = *my_data;
	*my_data = atof(argv[index]);
	return 0;
}

static int callback_questionAmount(void* data, int argc, char** argv, char** azColName) 
{
	int len = atoi(argv[0]);
	*(int*)data = len;
	return 0;
}

SqliteDataBase::SqliteDataBase()
{
	std::string dbFileName = "triviaDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	sqlite3_open(dbFileName.c_str(), &m_db);
	open();
}

bool SqliteDataBase::doesUserExist(string name)
{
	string sqlStatement_doesUserExist = "SELECT * FROM USERS WHERE NAME = \"" + name + "\";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(m_db, sqlStatement_doesUserExist.c_str(), callback_doesUserExist, nullptr, &errMessage);
	if (result) {
		result = false;
		return true;
	}
	return false;
}

bool SqliteDataBase::doesPasswordMatch(string name, string password)
{
	string sqlStatement_doesUserExist = "SELECT * FROM USERS WHERE NAME = \"" + name + "\" AND PASSWORD = \"" + password + "\";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(m_db, sqlStatement_doesUserExist.c_str(), callback_doesUserExist, nullptr, &errMessage);
	if (result) {
		result = false;
		return true;
	}
	return false;
}

void SqliteDataBase::addNewUser(string name, string password, string email)
{
	if (!doesUserExist(name)) {
		string sqlStatement_doesUserExist = "INSERT INTO USERS (NAME, PASSWORD, EMAIL) VALUES(\"" + name + "\", \"" + password + "\",\"" + email + "\"); ";
		string addToStatistics = "INSERT INTO STATISTICS (USER, TIME, GAMES, CORRECT_ANS, TOTAL_ANS) VALUES(\"" + name + "\"" + ", 0, 0, 0, 0); ";
		char* errMessage = nullptr;
		int res = sqlite3_exec(m_db, sqlStatement_doesUserExist.c_str(), callback_doesUserExist, nullptr, &errMessage);
		sqlite3_exec(m_db, addToStatistics.c_str(), nullptr, nullptr, &errMessage);
	}
}

std::vector<Question*>* SqliteDataBase::getQuestions(int questionAmount)
{
	string sqlStatement_getQuestions = "SELECT * FROM QUESTIONS LIMIT " + (std::to_string)(questionAmount) + ';';
	char* errMessage = nullptr;
	std::vector<Question*> *questions = new std::vector<Question*>();
	int res = sqlite3_exec(m_db, sqlStatement_getQuestions.c_str(), callback_qestions, questions, &errMessage);
	return questions;
}

float SqliteDataBase::getAveargeAnswerTime(string name)
{
	string sqlStatement_getQuestions = "SELECT * FROM STATISTICS WHERE USER = \"" +  name + "\";";
	char* errMessage = nullptr;
	float data = 1.0;
	sqlite3_exec(m_db, sqlStatement_getQuestions.c_str(), callback_stats, &data, &errMessage);
	return data;
}

int SqliteDataBase::getNumOfCorrectAnswers(string name)
{
	string sqlStatement_getQuestions = "SELECT * FROM STATISTICS WHERE USER = \"" + name + "\";";
	char* errMessage = nullptr;
	float data = 3.0;
	sqlite3_exec(m_db, sqlStatement_getQuestions.c_str(), callback_stats, &data, &errMessage);
	return data;
}

int SqliteDataBase::getNumOfTotalAnswers(string name)
{
	string sqlStatement_getQuestions = "SELECT * FROM STATISTICS WHERE USER = \"" + name + "\";";
	char* errMessage = nullptr;
	float data = 4.0;
	sqlite3_exec(m_db, sqlStatement_getQuestions.c_str(), callback_stats, &data, &errMessage);
	return data;
}

int SqliteDataBase::getNumOfPlayerGames(string name)
{
	string sqlStatement_getQuestions = "SELECT * FROM STATISTICS WHERE USER = \"" + name + "\";";
	char* errMessage = nullptr;
	float data = 2.0;
	sqlite3_exec(m_db, sqlStatement_getQuestions.c_str(), callback_stats, &data, &errMessage);
	return data;
}

list<string> SqliteDataBase::getAllUsers()
{
	string sqlStatement_getQuestions = "SELECT NAME FROM USERS";
	char* errMessage = nullptr;
	list<string> data;
	sqlite3_exec(m_db, sqlStatement_getQuestions.c_str(), callback_USERS, &data, &errMessage);
	return data;
}

void SqliteDataBase::updateUserStates(string userName,float games,int correctAmount,int totalAnswers, float averageAnswerTime)
{
	std::string updateReq = "UPDATE STATISTICS SET TIME = " + std::to_string(averageAnswerTime) +", GAMES = " + std::to_string(games) + ", CORRECT_ANS = " + std::to_string(correctAmount)+ ", TOTAL_ANS = " + std::to_string(totalAnswers) + " WHERE USER = \"" + userName + "\";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(m_db, updateReq.c_str(), nullptr, nullptr, &errMessage);
}

int SqliteDataBase::changePassword(std::string newPass, std::string email)
{
	std::string changePass = "UPDATE USERS SET PASSWORD = '" + newPass + "' WHERE EMAIL='" + email + "';";
	char* errMessage = nullptr;
	return sqlite3_exec(m_db, changePass.c_str(), nullptr, nullptr, &errMessage);
}

int SqliteDataBase::addQuestion(std::string question, std::string correct, std::string wrong1, std::string wrong2, std::string wrong3)
{
	int questionsAmount = getQuestionAmount();
	std::string addQ = "INSERT INTO QUESTIONS VALUES('" + std::to_string(questionsAmount + 1) + "', '" + question + + "', '" + correct + "', '" + wrong1 + "', '" + wrong2 + "', '" + wrong3 + "');";
	char* errMessage = nullptr;
	return sqlite3_exec(m_db, addQ.c_str(), nullptr, nullptr, &errMessage);
}

int SqliteDataBase::getQuestionAmount()
{
	int questionsAmount = 0;
	std::string currentAmount = "SELECT QUESTION_NUMBER FROM QUESTIONS ORDER BY QUESTION_NUMBER DESC LIMIT 1;";
	char* errMessage = nullptr;
	sqlite3_exec(m_db, currentAmount.c_str(), callback_questionAmount, &questionsAmount, &errMessage);
	return questionsAmount;
}

bool SqliteDataBase::open()
{

	char* errMessage = nullptr;
	string sqlStatement_USERS = "CREATE TABLE \"USERS\"(\"NAME\" TEXT NOT NULL, \"PASSWORD\" TEXT NOT NULL, \"EMAIL\" TEXT NOT NULL, PRIMARY KEY(\"NAME\"));";
	string sqlStatement_QUESTIONS = "CREATE TABLE \"QUESTIONS\"(\"QUESTION\" TEXT NOT NULL, \"ANS\" TEXT NOT NULL, \"WRONG1\" TEXT NOT NULL,\"WRONG2\" TEXT NOT NULL,\"WRONG3\" TEXT NOT NULL, PRIMARY KEY(\"QUESTION\"));";
	string sqlStatement_STATISTICS = "CREATE TABLE \"STATISTICS\"(\"USER\" TEXT NOT NULL,\"TIME\" TEXT NOT NULL,\"GAMES\" INT NOT NULL,\"CORRECT_ANS\" INT NOT NULL,\"TOTAL_ANS\" INT NOT NULL, FOREIGN KEY(\"USER\")REFERENCES USERS(\"NAME\"));";

	sqlite3_exec(m_db, sqlStatement_USERS.c_str(), nullptr, nullptr, &errMessage);
	sqlite3_exec(m_db, sqlStatement_QUESTIONS.c_str(), nullptr, nullptr, &errMessage);
	sqlite3_exec(m_db, sqlStatement_STATISTICS.c_str(), nullptr, nullptr, &errMessage);
	return true;
}
