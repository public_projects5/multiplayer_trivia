#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RequestHandlerFactory.h"
#include "RequestsResponses.h"

class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(RequestHandlerFactory* requestHandlerFactory, StatisticsManager* statisticManager , std::string username, RoomManager* roomMnanager);
	~MenuRequestHandler();
	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult handlerRequest(RequestInfo requestInfo);
	

private:
	LoggedUser* m_loggedUser;
	RequestHandlerFactory* m_hanlerFacory;
	RoomManager* m_roomManager;
	StatisticsManager* m_staticManager;
	RequestResult signout(RequestInfo requestInfo);
	RequestResult getRooms(RequestInfo requestInfo);
	RequestResult getPlayersInRoom(RequestInfo requestInfo);
	RequestResult getPersonalStates(RequestInfo requestInfo);
	RequestResult getHighScore(RequestInfo requestInfo);
	RequestResult joinRoom(RequestInfo requestInfo);
	RequestResult createRoom(RequestInfo requestInfo);
	RequestResult addQuestion(RequestInfo requestInfo);
};