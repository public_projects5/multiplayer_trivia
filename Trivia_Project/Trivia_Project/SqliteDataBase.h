#pragma once
#include "IDatabase.h"
#include <iostream>
#include <vector>
#include "RequestsResponses.h"

class SqliteDataBase : public IDatabase
{
public:
	SqliteDataBase();
	bool doesUserExist(string name);
	bool doesPasswordMatch(string name, string password);
	void addNewUser(string name, string password, string email);

	std::vector<Question*>* getQuestions(int questionAmount);
	float getAveargeAnswerTime(string);
	int getNumOfCorrectAnswers(string);
	int getNumOfTotalAnswers(string);
	int getNumOfPlayerGames(string);
	list<string> getAllUsers();
	bool open();
	void updateUserStates(string userName, float games, int correctAmount, int totalAnswers, float averageAnswerTime);

	//bounuses:
	int changePassword(std::string newPass, std::string email);
	int addQuestion(std::string question, std::string correct, std::string wrong1, std::string wrong2, std::string wrong3);
	int getQuestionAmount();

private:
	sqlite3* m_db;
};

