#pragma once
#include <iostream>
//#include "Request.h"
#include "json.hpp"
#include "RequestsResponses.h"
using json = nlohmann::json;


class JsonRequestPacketDeserializer
{
public:
	static LoginRequest  deserializeLoginRequest(const char* login);
	static SignupRequest deserializeSignupRequest(const char* signup);
	static GetPlayersInRoomRequest deserializeGetPlayerRequest(const char* getPlayer);
	static JoinRoomRequest deserializeJoinRoomRequest(const char* joinRoom);
	static CreateRoomRequest deserializeCreateRoomRequest(const char* createRoom);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(const char* submitAnswer);
	static NewPasswordRequest deserializeNewPasswordRequest(const char* newPass);
	static AddQuestionRequest deserializeAddQuestion(const char* addQuestion);
};