#include "RoomMemeberRequestHandler.h"

RoomMemeberRequestHandler::RoomMemeberRequestHandler(RequestHandlerFactory* requestHandlerFactory, std::string userName, RoomManager* roomMnanager, Room* room)
{
	m_handlerFanctory = requestHandlerFactory;
	m_roomManager = roomMnanager;
	m_room = room;
	m_loggedUser = new LoggedUser(userName);
}

RoomMemeberRequestHandler::~RoomMemeberRequestHandler()
{
	delete m_loggedUser;
}

bool RoomMemeberRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return requestInfo.id == LEAVE_ROOM_RESPONSE || requestInfo.id == GET_ROOM_STATE_RESPONSE;
}

RequestResult RoomMemeberRequestHandler::handlerRequest(RequestInfo requestInfo)
{
	RequestResult reqResult{};
	if (!isRequestRelevant(requestInfo)) {
		char* empty = new char();
		return *new RequestResult{ empty, this };    //enmpy struct
	}

	switch (requestInfo.id)
	{
	case CLOSE_ROOM_RESPONSE:
		break;
	case START_GAME_RESPONSE:
		break;
	case LEAVE_ROOM_RESPONSE:
		reqResult = leaveRoom(requestInfo);
		break;
	case GET_ROOM_STATE_RESPONSE:
		reqResult = getRoomStatse(requestInfo);
		break;
	default:
		break;
	}
	return reqResult;
}

RequestResult RoomMemeberRequestHandler::leaveRoom(RequestInfo reqInfo)
{
	RequestResult reqResult{};
	LeaveRoomResponse leaveRoomRes;
	ErrorResponse errorRes;

	leaveRoomRes.status = SUCCESS;
	

	std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(leaveRoomRes);
 	helper::copyBuffer(&reqResult.buffer, jsonResponse);
	reqResult.newHandler = m_handlerFanctory->createMenuRequestHandler(m_loggedUser->getUserName());   //back to menu
	if (m_room->getAllUsers().size() == 1)    //last one should close the door
		m_roomManager->deleteRoom(m_room->getId());
	else
		m_room->removeUser(m_loggedUser->getUserName());
	return reqResult;
}

RequestResult RoomMemeberRequestHandler::getRoomStatse(RequestInfo reqInfo)
{
	RequestResult reqResult{};
	GetRoomStatsResponse getRoomRes;
	roomData roomData;

	getRoomRes.status = SUCCESS;
	roomData = m_room->getRoom();
	if (roomData.isActive == -1) {
		reqResult.newHandler = this;
		getRoomRes.answerTimeOut = -1;
		getRoomRes.hasGameBegun = -1;
		getRoomRes.players = *new std::vector<std::string>();
		getRoomRes.questionCount = roomData.numOfQuestionsInGame;
	}

	else {
		
		if (roomData.isActive == 1) {    // if game was started by the admin
			reqResult.newHandler = m_handlerFanctory->createGameRequestHandler(m_loggedUser, m_room, 0);
		}
		else
			reqResult.newHandler = this;

		getRoomRes.answerTimeOut = roomData.timePerQuestion;
		getRoomRes.hasGameBegun = roomData.isActive;
		getRoomRes.players = m_room->getAllUsers();
		getRoomRes.questionCount = roomData.numOfQuestionsInGame;
	}

	std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(getRoomRes);
	helper::copyBuffer(&reqResult.buffer, jsonResponse);
	return reqResult;
}
