#include "Communicator.h"

Communicator::Communicator(RequestHandlerFactory* handlerFactory)
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	m_handlerFactory = handlerFactory;
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(m_serverSocket);
	}
	catch (...) {}
}

void Communicator::startHandlerRequest(const int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	//bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa));

	if (bind(m_serverSocket, (const struct sockaddr*)&sa, 16) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		std::cout << "Waiting for client connection request" << std::endl;
		bindAndListen();
	}
}

void Communicator::bindAndListen()
{
	// the main thread is only accepting clients 
		// and add then to the list of handlers
	// this accepts the client and create a specific socket from server to this client
	// the process will not continue until a client connects to the server
	SOCKET client_socket = accept(m_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	//add user to user's map
	LoginRequestHandler* loginHandler = m_handlerFactory->createLoginRequestHandler();

	m_client.insert({ client_socket, loginHandler });
	std::thread client(&Communicator::handlerNewClient, this, client_socket);    //added
	client.detach();
	// the function that handle the conversation with the client
}

void Communicator::handlerNewClient(SOCKET socket)
{
	std::string username = "";
	RequestResult reqResult;

	while (true) {
		std::string bufferForClient = "";
		RequestInfo reqInfo;

		//recv data
		int len = 0;
		std::string buffer = "";
		int typeCode = 0;
		try
		{
			typeCode = helper::getMessageTypeCode(socket);
			if (typeCode != SIGN_OUT_RESPONSE && typeCode != GET_ROOM_RESPONSE && typeCode != GET_HIGH_SCORE_RESPONSE &&
				typeCode != GET_PERSON_STATS_RESPONSE && typeCode != LEAVE_ROOM_RESPONSE && typeCode != CLOSE_ROOM_RESPONSE &&
				typeCode != GET_ROOM_STATE_RESPONSE && typeCode != START_GAME_RESPONSE && typeCode != GET_QUESTION_RESPONSE && 
				typeCode != LEAVE_GAME_RESPONSE && typeCode != GET_GAME_RESULTS_RESPONSE)
			{
				len = helper::getIntPartFromSocket(socket);
				buffer = helper::getStringPartFromSocket(socket, len);
			}
		}
		catch (const std::exception&)
		{
			return;
		}

		//create the RequestInfo struct
		reqInfo.id = typeCode;
		time_t now = time(0);

		// convert now to string form
		reqInfo.receivalTime = ctime(&now);
		reqInfo.buffer = const_cast<char*>(buffer.c_str());
		if (typeCode == 1) {
			username = JsonRequestPacketDeserializer::deserializeLoginRequest(reqInfo.buffer).username;
		}
		reqResult = m_client[socket]->handlerRequest(reqInfo);     //get requestResult struct

		m_client.erase(socket);
		m_client.insert({ socket, reqResult.newHandler });
	
		if (send(socket, reqResult.buffer, 5 + strlen(reqResult.buffer + 5), 0) == INVALID_SOCKET)     //send message to client
		{
			throw std::exception("Error while sending message to client");
		}
	}
}
