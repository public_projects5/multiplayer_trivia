#include "GameManager.h"

GameManager::GameManager(IDatabase* database)
{
	m_games = new std::vector<Game*>();
	m_database = database;
}

GameManager::~GameManager()
{
	delete m_games;
}

Game* GameManager::createGame(Room room)
{
	std::map<LoggedUser*, GameData*>* players = new std::map<LoggedUser*, GameData*>();
	std::vector<Question*> *questions = m_database->getQuestions(room.getRoom().numOfQuestionsInGame);
	std::vector<std::string> allRooms = room.getAllUsers();
	for (std::vector<std::string>::iterator it = allRooms.begin(); it != allRooms.end(); it++) {
		LoggedUser *currLogged = new LoggedUser(it->c_str());
		GameData* gameData = new GameData();
		gameData->currentQuestion = (*questions)[0];
		gameData->currentNumQuestion = 1;
		gameData->correctAnswerCount = 0;
		gameData->wrongAnswerCount = 0;
		gameData->averageAnswerTime = 0;
		players->insert({ currLogged, gameData });              //insert user to the list game
	}
	Game *game = new Game(questions, players, room.getId());
	m_games->push_back(game);
	return game;
}

void GameManager::deleteGame(Game* game)
{
	std::vector<Game*>::iterator it = std::find(m_games->begin(), m_games->end(), game);
	if (it != m_games->end()) {
		m_games->erase(it);
	}
}

void GameManager::updatePlayerResults(LoggedUser* loggedUser, PlayerResults playerResults)
{
	std::string userName = loggedUser->getUserName();
	float games = m_database->getNumOfPlayerGames(userName) + 1;
	float averageAnswerTime = m_database->getAveargeAnswerTime(userName);
	unsigned int correctAmount = m_database->getNumOfCorrectAnswers(userName);
	unsigned int totalAnswers = m_database->getNumOfTotalAnswers(userName);

	averageAnswerTime = (averageAnswerTime * (games - 1) + playerResults.averageAnswerTime) / games;    //const formula for updates average
	correctAmount += playerResults.correctAnswerCount;
	totalAnswers += (playerResults.correctAnswerCount + playerResults.wrongAnswerCount);

	m_database->updateUserStates(userName, games, correctAmount, totalAnswers, averageAnswerTime);
}
 
Game* GameManager::getGame(int id)
{
	for (int i = 0; i < m_games->size(); i++) {
		int currId = (*(m_games->begin() + i))->getGameId();
		if (currId == id)
			return *(m_games->begin() + i);
	}
}
