#pragma once
#include <string>
class LoggedUser
{
public:
	LoggedUser(std::string name);
	~LoggedUser();
	std::string getUserName()const;
private:
	std::string m_username;
};
