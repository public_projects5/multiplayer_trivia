#include "Helper.h"
#include <WinSock2.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

int helper::getMessageTypeCode(const SOCKET sc)
{
	char* data = new char[1 + 1];
	int res = recv(sc, data, 1, 0);
	std::cout << "code = " << (int)data[0] << std::endl;

	//std::cout << "code = " << (int)data[0] << std::endl;
	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	return (int)data[0];
}


int helper::getIntPartFromSocket(const SOCKET sc)
{
	std::string retVal = getPartFromSocket(sc, 4, 0);
	int res = std::atoi(retVal.c_str());
	return res;
	//int retAsInt = charBinaryToDecimal(retVal.c_str());
	//return retAsInt;
}

std::string helper::getStringPartFromSocket(SOCKET sc, const int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}


char* helper::getDataProtocol(std::string buffer, unsigned int typeCode)
{
	std::string len = getPaddedNumber(buffer.length(), 4);
	char* totalBuffer = new char[5 + buffer.length()];
	totalBuffer[0] = typeCode;
	for (int i = 0; i < 4; i++) {
		totalBuffer[i + 1] = len[i];
	}
	strcpy(totalBuffer + 5, buffer.c_str());

	//char* len = helper::bufferToLDecimalLen(buffer);
	//std::string message = std::to_string(typeCode) + len + buffer;
	return totalBuffer;
}

std::string helper::getPaddedNumber(const int num, const int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();
}

void helper::copyBuffer(char** dest, std::string src)
{
	*dest = new char[5 + src.length()];
	strcpy(*dest, src.c_str());
}

std::string helper::getPartFromSocket(const SOCKET sc, const int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

std::string helper::getPartFromSocket(const SOCKET sc, const int bytesNum, const int flags)
{
	if (bytesNum == 0)
	{
		return "";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);
	//std::cout << "code = " << (int)data[0] << std::endl;
	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}
	data[bytesNum] = 0;
	std::string received(data);
	delete[] data;
	return received;
}