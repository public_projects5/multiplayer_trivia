#pragma once
#include <iostream>
//#include "Response.h"
#include "RequestsResponses.h"
#include "json.hpp"
using json = nlohmann::json;

class JsonResponsePacketSerializer
{
public:
	static std::string serializeResponse(LoginResponse login);
	static std::string serializeResponse(SignupResponse signUp);
	static std::string serializeResponse(ErrorResponse error);
	static std::string serializeResponse(LogoutResponse logoutRes);
	static std::string serializeResponse(GetRoomsResponse getRoomRes);
	static char* serializeResponse(GetPlayersInRoomResponse getPlayersInRoomRes);
	static std::string serializeResponse(JoinRoomResponse joinRoomRes);
	static std::string serializeResponse(CreateRoomResponse createRoomRes);
	static std::string serializeResponse(GetPersonalStatsResponse getPersonalStatsRes);
	static std::string serializeResponse(GetHighScoreResponse getHighScoreRes);
	static std::string serializeResponse(ClostRoomResponse clostRoomRes);
	static std::string serializeResponse(StartGameResponse startGameRes);
	static std::string serializeResponse(GetRoomStatsResponse getRoomStatsRes);
	static std::string serializeResponse(LeaveRoomResponse leaveRoomRes);
	static std::string serializeResponse(GetGameResultsResponse getGameResultsRes);
	static std::string serializeResponse(SubmitAnswerResponse submitAnswerRes);
	static std::string serializeResponse(GetQuestionResponse getQuestionRes);
	static std::string serializeResponse(LeaveGameResponse leaveGameRes);
	static std::string serializeResponse(NewPasswordResponse newPassRes);
	static std::string serializeResponse(AddQuestionResponse addQuestionRes);
};
