#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase* db, StatisticsManager* statisticManager, LoginManager* loginManager, RoomManager* roomManager, GameManager* gameManager)
{
	m_database = db;
	m_loginManager = loginManager;
	m_statisticManager = statisticManager;
	m_roomManager = roomManager;
	m_gameManager = gameManager;

	m_counter = new std::vector<int>(4);
	std::thread printVec(&RequestHandlerFactory::printVector, this, m_counter);   //init thread for printing vector
	printVec.detach();
}

RequestHandlerFactory::~RequestHandlerFactory()
{
	delete m_statisticManager;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	LoginRequestHandler* loginRequestHandler = new LoginRequestHandler(this, m_loginManager);
	return loginRequestHandler;
}

LoginManager* RequestHandlerFactory::getLoginManager()
{
	return m_loginManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return *m_roomManager;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(std::string username)
{
	MenuRequestHandler* menuRequestHandler = new MenuRequestHandler(this, m_statisticManager, username, m_roomManager);
	return menuRequestHandler;
}

StatisticsManager& RequestHandlerFactory::getStatisticManager()
{
	return *m_statisticManager;
}

RoomMemeberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(Room* room, std::string username)
{
	RoomMemeberRequestHandler* menuRequestHandler = new RoomMemeberRequestHandler(this, username, m_roomManager, room);
	return menuRequestHandler;
}
GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser* loggedUser, Room* room, int premission)
{
	Game* game;
	if (premission == 1) //admin
		game = m_gameManager->createGame(*room);
	else   //member
		game = m_gameManager->getGame(room->getId());
	GameRequestHandler* gameRequestHandler = new GameRequestHandler(this, game, loggedUser, m_gameManager);
	return gameRequestHandler;
}

GameManager& RequestHandlerFactory::getGameManager()
{
	return *m_gameManager;
}

IDatabase* RequestHandlerFactory::getDateBase()
{
	return m_database;
}

std::vector<int>* RequestHandlerFactory::getVector()
{
	return m_counter;
}

void RequestHandlerFactory::printVector(std::vector<int>* toPrint) const
{
	if ((*m_counter)[0] > 0 && (*m_counter)[1] > 0 && (*m_counter)[2] > 0 && (*m_counter)[3] > 0) {
		for (std::vector<int>::iterator it = m_counter->begin(); it != m_counter->end(); it++) {
			std::cout << m_counter << " ";
		}
		std::cout << std::endl;
	}
	std::this_thread::sleep_for(3000ms);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(Room* room, std::string username)
{
	RoomAdminRequestHandler* menuRequestHandler = new RoomAdminRequestHandler(this, username, m_roomManager, room);
	return menuRequestHandler;
}
