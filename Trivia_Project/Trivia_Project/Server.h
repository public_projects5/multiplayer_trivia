#pragma once
#include "Communicator.h"
#include "SqliteDataBase.h"

class Server
{

public:
	Server(IDatabase* database);
	~Server();
	void run();

private:
	IDatabase* m_database;
	RequestHandlerFactory* m_handlerFactory;
	Communicator* m_communicator;
};