#pragma once
#include "IRequestHandler.h"
#include "RequestsResponses.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory* requestHandlerFactory, LoginManager* loginManager);
	~LoginRequestHandler();
	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult handlerRequest(RequestInfo requestInfo);

private:
	LoginManager* m_loginManager;
	RequestHandlerFactory* m_hanlerFacory;
	RequestResult login(RequestInfo RequestInfo);
	RequestResult signup(RequestInfo RequestInfo);
	RequestResult newPassword(RequestInfo requestInfo);
};
