#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory* requestHandlerFactory, StatisticsManager* statisticManager, std::string username, RoomManager* roomMnanager) : IRequestHandler()
{
    m_loggedUser = new LoggedUser(username);
    //m_staticManager = new StatisticsManager(requestHandlerFactory->getStatisticManager());
    m_staticManager = statisticManager;
    m_hanlerFacory = requestHandlerFactory;
    m_roomManager = roomMnanager;
}

MenuRequestHandler::~MenuRequestHandler()
{
    //delete m_roomManager;
    delete m_staticManager;
   // delete m_hanlerFacory;
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
    return requestInfo.id == GET_ROOM_RESPONSE || requestInfo.id == GET_HIGH_SCORE_RESPONSE || requestInfo.id == SIGN_OUT_RESPONSE
        || requestInfo.id == GET_PERSON_STATS_RESPONSE || requestInfo.id == JOIN_ROOM_RESPONSE ||
        requestInfo.id == CREATE_ROOM_RESPONSE || requestInfo.id == GET_PLAYERS_IN_ROOM || requestInfo.id == ADD_QUESTION;
}

RequestResult MenuRequestHandler::handlerRequest(RequestInfo requestInfo)
{
    RequestResult reqResult{};

    if (!isRequestRelevant(requestInfo)) {
        char* empty = new char();
        return *new RequestResult{ empty, this };    //enmpy struct
    }
    std::vector<int> *getVal = m_hanlerFacory->getVector();
    switch (requestInfo.id)
    {
    case SIGN_OUT_RESPONSE:
        reqResult = signout(requestInfo);
        break;
    case GET_ROOM_RESPONSE:
        reqResult = getRooms(requestInfo);
        break;
    case GET_HIGH_SCORE_RESPONSE:
        reqResult = getHighScore(requestInfo);
        break;
    case GET_PERSON_STATS_RESPONSE:
        (*getVal)[3]++;
        reqResult = getPersonalStates(requestInfo);
        break;
    case JOIN_ROOM_RESPONSE:
        (*getVal)[1]++;
        reqResult = joinRoom(requestInfo);
        break;
    case CREATE_ROOM_RESPONSE:
        (*getVal)[0]++;
        reqResult = createRoom(requestInfo);
        break;
    case GET_PLAYERS_IN_ROOM:
        reqResult = getPlayersInRoom(requestInfo);
        break;
    case ADD_QUESTION:
        (*getVal)[2]++;
        reqResult = addQuestion(requestInfo);
        break;
    default:
        break;
    }
    return reqResult;
}


RequestResult MenuRequestHandler::signout(RequestInfo requestInfo)
{
    RequestResult reqResult;
    LoginManager *lManager = m_hanlerFacory->getLoginManager();
    reqResult.buffer = nullptr;
    reqResult.buffer = requestInfo.buffer;         //signout request's buffer contains only the name
    lManager->logout(m_loggedUser->getUserName());
    SignupResponse response;
    response.status = SUCCESS;

    std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(response);
    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    reqResult.newHandler = m_hanlerFacory->createLoginRequestHandler();
    return reqResult;
}


RequestResult MenuRequestHandler::getRooms(RequestInfo requestInfo)
{
    RequestResult reqResult;
    reqResult.buffer = nullptr;      //there is no buffer in this requestInfo
    std::vector<roomData> rooms = m_roomManager->getRooms();
    std::string names = "";

    GetRoomsResponse response;
    response.status = SUCCESS;
    response.rooms = rooms;

    std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(response);
    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    reqResult.newHandler = this;

    return reqResult;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo requestInfo)
{
    RequestResult reqResult;
    reqResult.buffer = nullptr;
    std::string jsonResponse = "";
    GetPlayersInRoomRequest getPlayerReq = JsonRequestPacketDeserializer::deserializeGetPlayerRequest(requestInfo.buffer);
    Room* room = m_hanlerFacory->getRoomManager().getRoom(getPlayerReq.roomId);
    std::vector<std::string> allUsers;
    if(room != nullptr)
        allUsers = room->getAllUsers();

    GetPlayersInRoomResponse response;
    response.status = (room != nullptr) ? 1 : 0;
    if (room == nullptr) {
        ErrorResponse errorRes;
        errorRes.message = "*can't create this room!";
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(errorRes);  
        reqResult.newHandler = this;
    }
    else{
        response.players = allUsers;
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(response);
        if(m_loggedUser->getUserName() == allUsers.begin()->c_str())     //the first item in vector is edmin! else- member..
            reqResult.newHandler = m_hanlerFacory->createRoomAdminRequestHandler(room,this->m_loggedUser->getUserName());
        else
            reqResult.newHandler = m_hanlerFacory->createRoomMemberRequestHandler(room, this->m_loggedUser->getUserName());
    }
    
    helper::copyBuffer(&reqResult.buffer, jsonResponse);

    return reqResult;
}

RequestResult MenuRequestHandler::getPersonalStates(RequestInfo requestInfo)
{
    RequestResult reqResult;
    reqResult.buffer = nullptr;
    std::vector<std::string> statistics = m_staticManager->getUserStatistics(m_loggedUser->getUserName());

    GetPersonalStatsResponse response;
    response.status = SUCCESS;
    response.statistics = statistics;

    std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(response);
    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    reqResult.newHandler = this;

    return reqResult;
}

RequestResult MenuRequestHandler::getHighScore(RequestInfo requestInfo)
{
    RequestResult reqResult;
    reqResult.buffer = nullptr;
    std::vector<std::string> bestPlayers = m_staticManager->getHighScore();
    std::string names = "";

    GetHighScoreResponse response;
    response.status = SUCCESS;
    response.statistics = bestPlayers;

    std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(response);
    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    reqResult.newHandler = this;

    return reqResult;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo requestInfo)
{
    RequestResult reqResult;
    reqResult.buffer = nullptr;
    std::string jsonResponse = "";
    JoinRoomRequest joinRoomReq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(requestInfo.buffer);
    Room* room = m_roomManager->getRoom(joinRoomReq.roomId);
    JoinRoomResponse response;
    if (room != nullptr && room->getRoom().isActive != 1) {
        room->addUser(*m_loggedUser);
        response.status = 1;
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(response);
    }
    else {
        ErrorResponse errorRes;
        errorRes.message = "*room not found or active right now";
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(errorRes);
    }

    reqResult.newHandler = this;

    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    
    return reqResult;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo requestInfo)
{
    RequestResult reqResult;
    reqResult.buffer = nullptr;
    CreateRoomRequest createRoomReq = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(requestInfo.buffer);
    roomData roomD;
    std::string jsonResponse;
    roomD.timePerQuestion = createRoomReq.answerTimeout;
    roomD.maxPlayers = createRoomReq.maxUsers;
    roomD.numOfQuestionsInGame = createRoomReq.questionCount;
    roomD.name = createRoomReq.roomName;
    int questionAmount = m_hanlerFacory->getDateBase()->getQuestionAmount();

    if (questionAmount < roomD.numOfQuestionsInGame) {
        ErrorResponse errorRes;
        errorRes.message = "*There are not enough questions in the database";
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(errorRes);
    }
    else {
        Room* room = m_roomManager->createRoom(*m_loggedUser, roomD);
        CreateRoomResponse createRoomResponse;
        createRoomResponse.status = SUCCESS;
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(createRoomResponse);
    }
    
    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    reqResult.newHandler = this;

    return reqResult;
}

RequestResult MenuRequestHandler::addQuestion(RequestInfo requestInfo)
{
    RequestResult reqResult;
    reqResult.buffer = nullptr;
    AddQuestionRequest addQuestionReq = JsonRequestPacketDeserializer::deserializeAddQuestion(requestInfo.buffer);

    m_hanlerFacory->getDateBase()->addQuestion(addQuestionReq.question, addQuestionReq.correct, addQuestionReq.wrong1, addQuestionReq.wrong2, addQuestionReq.wrong3);

    AddQuestionResponse addQestionRes;
    addQestionRes.stauts = SUCCESS;

    std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(addQestionRes);
    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    reqResult.newHandler = this;

    return reqResult;
}
