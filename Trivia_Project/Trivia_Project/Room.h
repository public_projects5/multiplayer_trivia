#pragma once
#include <string>
#include <vector>
#include "LoggedUser.h"
#include "RequestsResponses.h"

class Room
{
public:
	Room(std::string name, unsigned int maxPlayers, unsigned int numOfQuestionsInGame, unsigned int timePerQuestion);
	Room(Room* roomOther);
	void addUser(LoggedUser loggedUser);
	void removeUser(LoggedUser loggedUser);
	std::vector<std::string> getAllUsers();
	std::vector<LoggedUser>* getUsers();
	roomData getRoom() const;
	int getId();
	void setActivity(int activity);

private:
	roomData m_metadata;
	std::vector<LoggedUser>* m_users;
};

