#include "Question.h"
#include <string>
Question::Question()
{
}

Question::Question(std::string question, std::string correctAns, std::string wrong1, std::string wrong2, std::string wrong3)
{
	m_possibleAnswers = new std::vector<std::string>();
	std::vector<std::string> possibleAns;
	m_correctAnswer = correctAns;
	m_question = question;
	m_answerNumber = 1 + (std::rand() % (4 - 1 + 1));
	std::cout << "answer: " + std::to_string(m_answerNumber) << std::endl;
	switch (m_answerNumber)
	{
	case 1:
		m_possibleAnswers->push_back(wrong1);
		m_possibleAnswers->push_back(wrong2);
		m_possibleAnswers->push_back(wrong3);
		m_possibleAnswers->push_back(correctAns);
		//m_answerNumber = 4;
		break;
	case 2:
		m_possibleAnswers->push_back(wrong1);
		m_possibleAnswers->push_back(wrong2);
		m_possibleAnswers->push_back(correctAns);
		m_possibleAnswers->push_back(wrong3);
		//m_answerNumber = 3;
		break;
	case 3:
		m_possibleAnswers->push_back(wrong2);
		m_possibleAnswers->push_back(correctAns);
		m_possibleAnswers->push_back(wrong1);
		m_possibleAnswers->push_back(wrong3);
		//m_question = 2;
		break;
	case 4:
		m_possibleAnswers->push_back(correctAns);
		m_possibleAnswers->push_back(wrong3);
		m_possibleAnswers->push_back(wrong1);
		m_possibleAnswers->push_back(wrong2);
		//m_answerNumber = 1;
		break;
	default:
		break;
	}
}

Question::~Question()
{
	int x = 0;
	//m_possibleAnswers->push_back("222");
	//delete m_possibleAnswers;
}

std::string Question::getQuestion() const
{
	return m_question;
}

std::vector<std::string> Question::getPossibleAnswer() const
{
	return *m_possibleAnswers;
}

std::string Question::getCorrectAnswer() const
{
	return m_correctAnswer;
}

int Question::getCorrectAnswerNum() const
{
	return m_answerNumber;
}

void Question::setCorrectInteger(int num)
{
	m_correctAnswer = num;
}
