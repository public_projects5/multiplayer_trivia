#include "LoggedUser.h"

LoggedUser::LoggedUser(std::string name)
{
	this->m_username = name;
}

LoggedUser::~LoggedUser()
{
}

std::string LoggedUser::getUserName() const
{
	return m_username;
}