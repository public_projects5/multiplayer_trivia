#pragma once
#include <vector>
#include <string>
#include "IDatabase.h"
class StatisticsManager
{
public:
	StatisticsManager(IDatabase* database);
	std::vector<std::string> getHighScore();
	std::vector<std::string> getUserStatistics(const std::string);
private:
	IDatabase* m_database;
	int calcScore(const string name);
};

