#pragma once
#include <string>
#include "IDatabase.h"
#include "LoggedUser.h"
#include <vector>

using namespace std;

class LoginManager
{
public:
	LoginManager(IDatabase* database);  
	~LoginManager();
	int signup(string name, string password, string email);
	int login(string name, string password);
	void logout(string name);
	boolean inPlayerActive(std::string username);
	int newPassword(std::string newPass, std::string email);

private:
	IDatabase* m_database;
	vector<LoggedUser> *m_loggedUsers;
};