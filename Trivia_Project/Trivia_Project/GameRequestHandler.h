#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "StatisticsManager.h"
#include "RequestHandlerFactory.h"
#include "RequestsResponses.h"
#include "GameManager.h"

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(RequestHandlerFactory* handlerFanctory, Game* game, LoggedUser* loggedUser, GameManager* gameManager);
	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult handlerRequest(RequestInfo requestInfo);

private:
	Game* m_game;
	LoggedUser* m_loggedUser;
	GameManager* m_gameManager;
	RequestHandlerFactory* m_handlerFanctory;
	RequestResult leave(RequestInfo requestInfo);
	RequestResult getQuestions(RequestInfo requestInfo);
	RequestResult submitAnswer(RequestInfo requestInfo);
	RequestResult getResults(RequestInfo requestInfo);
};

