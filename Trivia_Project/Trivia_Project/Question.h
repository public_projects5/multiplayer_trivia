#pragma once
#include<iostream>
#include<vector>
#include <utility>

class Question
{
public:
	Question();
	Question(std::string question, std::string correctAns, std::string wrong1, std::string wrong2, std::string wrong3);
	~Question();
	std::string getQuestion()const;
	std::vector<std::string> getPossibleAnswer()const;
	std::string getCorrectAnswer()const;
	int getCorrectAnswerNum()const;
	void setCorrectInteger(int num);

private:
	std::string m_question;
	std::string m_correctAnswer;
	int m_answerNumber;
	std::vector<std::string> *m_possibleAnswers;
};

