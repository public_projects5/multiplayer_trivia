#pragma once
#include <iostream>
#include "WSAInitializer.h"
#include "Server.h"
#include "LoginManager.h"
#include "IDatabase.h"
#include "SqliteDataBase.h"

int main(void)
{
	WSAInitializer wsaInit;
	SqliteDataBase sqliteDataBase1;

	Server server(&sqliteDataBase1);
	server.run();

	return 0;
}
