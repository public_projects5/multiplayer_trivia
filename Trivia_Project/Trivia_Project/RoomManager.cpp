#include "RoomManager.h"

RoomManager::RoomManager()
{
	m_rooms = new std::map<unsigned int, Room>();
}

Room* RoomManager::createRoom(LoggedUser loggenUser, roomData roomD)
{
	Room room(roomD.name, roomD.maxPlayers, roomD.numOfQuestionsInGame, roomD.timePerQuestion);

	room.addUser(loggenUser);
	m_rooms->insert({ room.getRoom().id,room });
	return &room;
}

void RoomManager::deleteRoom(unsigned int ID)
{
	std::map<unsigned int, Room>::iterator it;
	for (it = m_rooms->begin(); it != m_rooms->end(); it++)
	{
		if (it->first == ID) {
			m_rooms->erase(it);
			break;
		}
	}
}

unsigned int RoomManager::getRoomState(unsigned int ID)
{
	std::map<unsigned int, Room>::iterator it;

	for (it = m_rooms->begin(); it != m_rooms->end(); it++)
	{
		if (it->first == ID) {
			return it->second.getRoom().isActive;
		}
	}
}

void RoomManager::clearRooms()
{
	std::map<unsigned int, Room>::iterator it;

	if (m_rooms == nullptr)
		return;
	for (it = m_rooms->begin(); it != m_rooms->end(); it++)
	{
		if (it->second.getAllUsers().empty())
		{
			m_rooms->erase(it);
		}
	}
}

std::vector<roomData> RoomManager::getRooms()
{
	std::map<unsigned int, Room>::iterator it;
	std::vector<roomData> rooms;
	if (m_rooms == nullptr)
		return rooms;
	for (it = m_rooms->begin(); it != m_rooms->end(); it++)
	{
			rooms.push_back(it->second.getRoom());
	}
	return rooms;
}

Room* RoomManager::getRoom(unsigned int roomID)
{
	std::map<unsigned int, Room>::iterator it;

	for (it = m_rooms->begin(); it != m_rooms->end(); it++)
	{
		if (it->first == roomID) {
			return &it->second;
		}
	}
	return nullptr;
}

/*
void RoomManager::addUser(std::string name, unsigned int roomID)
{
	
}*/
