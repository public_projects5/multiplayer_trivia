import json
import requests
import sqlite3

conn = sqlite3.connect('triviaDB.sqlite')
c = conn.cursor()

# ******CREATE TABLE*********    using this line for the first run only

# drop table for re-creating- use this line only when you use the creating line!!
c.execute("DROP TABLE QUESTIONS;")
conn.commit()

c.execute("""
CREATE TABLE QUESTIONS(
QUESTION_NUMBER INTEGER,
QUESTION text,
CORRECT text,
WRONG1 text,
WRONG2 text,
WRONG3 text
)""")

# Get dummy data using an API
res = requests.get("https://opentdb.com/api.php?amount=30")
mount_of_questions_to_DB = 10

# Convert data to dict
data = json.loads(res.text)

questions = data.values()
count_questions = 1

for element in questions:     # there is two element in response
    if element != 0:     # we need the second element- that not include the 0 at the beginning
        for q in element:     # loop on each question- type of q is dict
            if mount_of_questions_to_DB == 0:
                break
                # in the trivia project we need questions with 4 optionals for answer
            if(len(q["incorrect_answers"]) == 3):
                mount_of_questions_to_DB -= 1
                c.execute("INSERT INTO QUESTIONS VALUES " + "('" + str(count_questions) + "', '" + q["question"] + "', '" + q["correct_answer"] + "', '" + q["incorrect_answers"][0] + "', '" + q["incorrect_answers"][1] + "', '" + q["incorrect_answers"][2] + "')")
                conn.commit()
                count_questions += 1
conn.close()
