#pragma once
#include <iostream>
#include "sqlite3.h"
#include <io.h>
#include "Question.h"
#include <list>
#include "RequestsResponses.h"
using namespace std;
class IDatabase
{
public:
	virtual bool doesUserExist(string name) = 0;
	virtual bool doesPasswordMatch(string name, string password) = 0;
	virtual void addNewUser(string name, string password, string email) = 0;
	virtual std::vector<Question*>* getQuestions(const int questionAmount) = 0;
	virtual float getAveargeAnswerTime(string) = 0;
	virtual int getNumOfCorrectAnswers(string) = 0;
	virtual int getNumOfTotalAnswers(string) = 0;
	virtual int getNumOfPlayerGames(string) = 0;
	 //tempdsadasd
	virtual list<string> getAllUsers() = 0;
	virtual void updateUserStates(string userName, float games, int correctAmount, int totalAnswers, float averageAnswerTime) = 0;
	//bounuses:
	virtual int changePassword(std::string newPass, std::string email) = 0;
	virtual int addQuestion(std::string question, std::string correct, std::string wrong1, std::string wrong2, std::string wrong3) = 0;
	virtual int getQuestionAmount() = 0;
};
