#include "Game.h"

Game::Game(std::vector<Question*>* questions, std::map<LoggedUser*, GameData*>* players, int roomId)
{
	std::map<LoggedUser*, GameData*>::iterator it1 = players->begin();
	for (it1; it1 != players->end(); it1++) {
		m_actives.push_back(it1->first);
	}
	m_question = questions;
	m_players = players;
	m_roomId = roomId;
}

Question Game::getQuestionForUser(LoggedUser loggedUser)
{
	std::map<LoggedUser*, GameData*>::iterator it = m_players->begin();
	for (it; it != m_players->end(); it++) {
		if (it->first->getUserName() == loggedUser.getUserName()) {
			GameData gameDate = *it->second;
			if(gameDate.currentNumQuestion <= m_question->size())
				return *it->second->currentQuestion;
		}
	}
	return *new Question("", "", "", "", "");
}

void Game::submitAnswer(LoggedUser loggedUser, int answer, int time)
{
	std::map<LoggedUser*, GameData*>::iterator it = m_players->begin();
	for (it; it != m_players->end(); it++) {
		if (it->first->getUserName() == loggedUser.getUserName()) {
			GameData *gameDate = it->second;
			if (it->second->currentQuestion->getCorrectAnswerNum() == answer)
				it->second->correctAnswerCount++;
			else
				it->second->wrongAnswerCount++;
			int currentTime = it->second->averageAnswerTime;
			double totalGames = it->second->correctAnswerCount + it->second->wrongAnswerCount;
			it->second->averageAnswerTime = (currentTime * totalGames + time) / (totalGames + 1);
			if (it->second->currentNumQuestion < m_question->size()) 
				it->second->currentQuestion = (*m_question)[it->second->currentNumQuestion];    //next question object
			it->second->currentNumQuestion++;     //next index question
			break;
		}
	}
}

int Game::getGameId()
{
	return m_roomId;
}

std::vector<PlayerResults> Game::getResults()
{
	std::vector<PlayerResults>* res = new std::vector<PlayerResults>();
	if(m_actives.size() > 0){
		return *res;
	}
	
	std::map<LoggedUser*, GameData*>::iterator it = m_players->begin();
	for (it; it != m_players->end(); it++) {
		PlayerResults currRes({it->first->getUserName(), it->second->correctAnswerCount, it->second->wrongAnswerCount, it->second->averageAnswerTime});
		res->push_back(currRes);
	}
	return *res;
}

void Game::removePlayer(LoggedUser loggedUser)
{
	for (int i = 0; i < m_actives.size(); i++) {
		if (m_actives[i]->getUserName() == loggedUser.getUserName())
			m_actives.erase(m_actives.begin() + i);
	}
}