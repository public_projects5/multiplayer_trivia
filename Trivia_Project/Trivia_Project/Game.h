#pragma once
#include<map>
#include "Question.h"
#include "LoggedUser.h"
#include "RequestsResponses.h"
#include <mutex>

class Game
{
public:
	Game(std::vector<Question*>* questions, std::map<LoggedUser*, GameData*> *players, int roomId);
	Question getQuestionForUser(LoggedUser loggedUser);
	void submitAnswer(LoggedUser loggedUser, int answer, int time);
	int getGameId();
	std::vector<PlayerResults> getResults();
	void removePlayer(LoggedUser loggedUser);

private:
	std::vector<Question*>* m_question;
	std::vector<LoggedUser*> m_actives;
	std::map<LoggedUser*, GameData*> *m_players;
	int m_roomId;
};

