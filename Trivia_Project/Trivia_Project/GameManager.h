#pragma once
#include "IDatabase.h"
#include <iostream>
#include <vector>
#include "Game.h"
#include "Room.h"
#include "Question.h"
#include <algorithm>

class GameManager
{
public:
	GameManager(IDatabase* database);
	~GameManager();
	Game* createGame(Room room);
	void deleteGame(Game* game);
	void updatePlayerResults(LoggedUser* loggedUser, PlayerResults playerResults);
	Game* getGame(int id);

private:
	IDatabase* m_database;
	std::vector<Game*> *m_games;
};

