#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory* requestHandlerFactory, std::string userName, RoomManager* roomMnanager, Room* room)
{
	m_handlerFanctory = requestHandlerFactory;
	m_roomManager = roomMnanager;
	m_room = room;
	m_loggedUser = new LoggedUser(userName);
}

RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
	delete m_loggedUser;
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return requestInfo.id == CLOSE_ROOM_RESPONSE || requestInfo.id == START_GAME_RESPONSE ||
		requestInfo.id == GET_ROOM_STATE_RESPONSE;
}

RequestResult RoomAdminRequestHandler::handlerRequest(RequestInfo requestInfo)
{
	RequestResult reqResult{};
	if (!isRequestRelevant(requestInfo)) {
		char* empty = new char();
		return *new RequestResult{ empty, this };    //enmpy struct
	}
	switch (requestInfo.id)
	{
	case CLOSE_ROOM_RESPONSE:
		reqResult = closeRoom(requestInfo);
		break;
	case START_GAME_RESPONSE:
		reqResult = startGame(requestInfo);
		break;
	case GET_ROOM_STATE_RESPONSE:
		reqResult = getRoomStatse(requestInfo);
		break;
	default:
		break;
	}
	return reqResult;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo reqInfo)
{
	RequestResult reqResult{};
	StartGameResponse startGameRes;

	m_room->setActivity(1);     //change the activity to be 1

	startGameRes.status = SUCCESS;
	Room* roomCpy = new Room(m_room);


	std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(startGameRes);
	helper::copyBuffer(&reqResult.buffer, jsonResponse);

	reqResult.newHandler = m_handlerFanctory->createGameRequestHandler(m_loggedUser, roomCpy, 1);
	return reqResult;
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo reqInfo)
{
	RequestResult reqResult{};
	ClostRoomResponse closeRoomRes;
	m_room->setActivity(-1);     //-1 means that admin closed the room
	closeRoomRes.status = SUCCESS;
	std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(closeRoomRes);
	helper::copyBuffer(&reqResult.buffer, jsonResponse);
	reqResult.newHandler = m_handlerFanctory->createMenuRequestHandler(m_loggedUser->getUserName());    //back to menu
	
	if(m_room->getAllUsers().size() == 1)
		m_roomManager->deleteRoom(m_room->getId());
	else
		m_room->removeUser(*m_loggedUser);
	return reqResult;
}

RequestResult RoomAdminRequestHandler::getRoomStatse(RequestInfo reqInfo)
{
	RequestResult reqResult{};
	GetRoomStatsResponse getRoomRes;
	roomData roomData;

	getRoomRes.status = SUCCESS;
	roomData = m_room->getRoom();
	getRoomRes.answerTimeOut = roomData.timePerQuestion;
	getRoomRes.hasGameBegun = 0;    
	getRoomRes.players = m_room->getAllUsers();
	getRoomRes.questionCount = roomData.numOfQuestionsInGame;

	std::string jsonResponse = JsonResponsePacketSerializer::serializeResponse(getRoomRes);
	helper::copyBuffer(&reqResult.buffer, jsonResponse);
	reqResult.newHandler = this;
	return reqResult;
}
