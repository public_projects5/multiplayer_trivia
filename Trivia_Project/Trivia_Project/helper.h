#pragma once
#include <vector>
#include <string>
#include <WinSock2.h>
//#include "Response.h"
#include "RequestsResponses.h"

#define LEM_BYTE 4

class helper
{
public:
	static int getMessageTypeCode(const SOCKET sc);
	static int getIntPartFromSocket(const SOCKET sc);
	static std::string getStringPartFromSocket(SOCKET sc, const int bytesNum);
	static char* getDataProtocol(std::string buffer, unsigned int typeCode);
	static std::string getPaddedNumber(const int num, const int digits);
	static void copyBuffer(char** dest, std::string src);

private:
	static std::string getPartFromSocket(const SOCKET sc, const int bytesNum);
	static std::string getPartFromSocket(const SOCKET sc, const int bytesNum, const int flags);
};