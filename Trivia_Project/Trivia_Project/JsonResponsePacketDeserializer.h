#include <iostream>
#include "Request.h"
#include "json.hpp"
using json = nlohmann::json;


class JsonResponsePacketSerializer
{
public:
	static LoginRequest  deserializeLoginRequest(char* login);
	static SignupRequest deserializeSignupRequest(char* signup);

};