#pragma comment (lib, "ws2_32.lib")
#include "Server.h"

Server::Server(IDatabase* database)
{
	m_database = database;
	m_handlerFactory = new RequestHandlerFactory(m_database, new StatisticsManager(database), new LoginManager(m_database), new RoomManager(), new GameManager(m_database));
	m_communicator = new Communicator(m_handlerFactory);
}

Server::~Server()
{
	delete m_handlerFactory;
	delete m_communicator;
}


void Server::run()
{
	m_communicator->startHandlerRequest(8826);
}