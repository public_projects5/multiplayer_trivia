#pragma once
#include <iostream>
#include <WinSock2.h>
#include <stdio.h>
#include <thread>
#include <condition_variable>
#include <map>
#include <string>
#include <exception>
#include <queue>
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "helper.h"
//#include "Request.h"
//#include "Response.h"
#include "RequestsResponses.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"


class Communicator
{
public:
	Communicator(RequestHandlerFactory* handlerFactory);
	~Communicator();
	void startHandlerRequest(const int port);

private:
	SOCKET m_serverSocket;
	std::map <SOCKET, IRequestHandler*> m_client;
	RequestHandlerFactory* m_handlerFactory;
	std::mutex _mMessages;
	std::condition_variable _cvMessages;

	void bindAndListen();
	void handlerNewClient(SOCKET socket);
	std::string getError(const int typeCode);   //returns string's error by the typeCode
};
