#include "LoginRequestHandler.h"

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* requestHandlerFactory, LoginManager* loginManager) : IRequestHandler()
{
    m_loginManager = loginManager;
    m_hanlerFacory = requestHandlerFactory;
}

LoginRequestHandler::~LoginRequestHandler()
{
}


bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
    return requestInfo.id == LOGIN_RESPONSE || requestInfo.id == SIGN_UP_RESPONSE || requestInfo.id == NEW_PASSWORD;
}

RequestResult LoginRequestHandler::handlerRequest(RequestInfo requestInfo)
{
    RequestResult reqResult;
    if (!isRequestRelevant(requestInfo))     //return empty result
    {
        char* empty = new char();
        return *new RequestResult{ empty, this };    //enmpy struct
    }

    switch (requestInfo.id)
    {
    case LOGIN_RESPONSE:
        reqResult = login(requestInfo);
        break;
    case SIGN_UP_RESPONSE:
        reqResult = signup(requestInfo);
        break;
    case NEW_PASSWORD:
        reqResult = newPassword(requestInfo);
    default:
        break;
    }
    return reqResult;
}

RequestResult LoginRequestHandler::login(RequestInfo requestInfo)
{
    RequestResult reqResult = *new RequestResult{};
    LoginRequest loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(requestInfo.buffer);
    std::string jsonResponse = "";
    reqResult.buffer = nullptr;
    reqResult.newHandler = this;
    int res = 0;
    boolean isArleadyOnline = m_loginManager->inPlayerActive(loginReq.username);
    if(!isArleadyOnline)
    res = m_loginManager->login(loginReq.username, loginReq.password);             //login in database
    
    LoginResponse loginResponse;
    if (res == 0 || isArleadyOnline) {
        ErrorResponse errorRes;
        errorRes.message = "*Username and password are not matches!";
        if(isArleadyOnline)
            errorRes.message = "*This user is already online!";
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(errorRes);     //response to buffer
        loginResponse.status = 0;
        reqResult.newHandler = this;
    }
    else {
        loginResponse.status = 1;
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(loginResponse);
        reqResult.newHandler = m_hanlerFacory->createMenuRequestHandler(loginReq.username);
    }
    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    return reqResult;
}

RequestResult LoginRequestHandler::signup(RequestInfo requestInfo)
{
    RequestResult reqResult;
    LoginManager *lManager = m_hanlerFacory->getLoginManager();
    SignupRequest signupReq = JsonRequestPacketDeserializer::deserializeSignupRequest(requestInfo.buffer);
    std::string jsonResponse = "";
    SignupResponse signupResponse;

    reqResult.buffer = nullptr;
    reqResult.buffer = requestInfo.buffer;
    int res = lManager->signup(signupReq.username, signupReq.password, signupReq.email);
    
    signupResponse.status = res;
    if (res == 0) {
        ErrorResponse errorRes;
        errorRes.message = "*Username is already exist!";
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(errorRes);     //response to buffer
        reqResult.newHandler = this;
    }
    else {
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(signupResponse);
        reqResult.newHandler = m_hanlerFacory->createMenuRequestHandler(signupReq.username);
    }

    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    return reqResult;
}

RequestResult LoginRequestHandler::newPassword(RequestInfo requestInfo)
{
    RequestResult reqResult = *new RequestResult{};
    NewPasswordRequest newPass = JsonRequestPacketDeserializer::deserializeNewPasswordRequest(requestInfo.buffer);
    std::string jsonResponse = "";
    reqResult.buffer = nullptr;
    reqResult.newHandler = this;

    NewPasswordResponse newPassResponse;

    int res = m_loginManager->newPassword(newPass.newPassword, newPass.email);

    if (res == 0) {
        ErrorResponse errorRes;
        errorRes.message = "*something wrong!";
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(errorRes);     //response to buffer
        newPassResponse.status = 0;
    }
    else {
        newPassResponse.status = 1;
        jsonResponse = JsonResponsePacketSerializer::serializeResponse(newPassResponse);
    }
    reqResult.newHandler = this;

    helper::copyBuffer(&reqResult.buffer, jsonResponse);
    return reqResult;
}
