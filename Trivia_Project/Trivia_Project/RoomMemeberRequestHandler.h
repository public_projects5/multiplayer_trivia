#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RequestHandlerFactory.h"
#include "RequestsResponses.h"

class RequestHandlerFactory;

class RoomMemeberRequestHandler : public IRequestHandler
{
public:
	RoomMemeberRequestHandler(RequestHandlerFactory* requestHandlerFactory, std::string userName,  RoomManager* roomMnanager, Room* room);
	~RoomMemeberRequestHandler();
	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult handlerRequest(RequestInfo requestInfo);

protected:
	RequestResult leaveRoom(RequestInfo reqInfo);
	RequestResult getRoomStatse(RequestInfo reqInfo);

private:
	Room* m_room;
	LoggedUser* m_loggedUser;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handlerFanctory;
};

