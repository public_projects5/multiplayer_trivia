#include "StatisticsManager.h"
#include <map>
#include <algorithm>

bool sortByVal(const pair<string, int>& a,
    const pair<string, int>& b)
{
    return (a.second < b.second);
}



StatisticsManager::StatisticsManager(IDatabase* database) : m_database(database)
{
}

std::vector<std::string> StatisticsManager::getHighScore()
{
    list<string> players = m_database->getAllUsers();
    std::vector<std::string> top_5;
    vector<pair<string, int>> vec;

    int size = players.size();
    for (int i = 0; i < size; i++)
    {

        vec.push_back({ players.back(), calcScore(players.back())});
        players.pop_back();
    }

    sort(vec.begin(), vec.end(), sortByVal);
    int count = 0;
    while(vec.size() > 0 &&  count < 5)
    {
        top_5.push_back(vec.back().first);
        vec.pop_back();
        count++;
    }

    return top_5;
}

std::vector<std::string> StatisticsManager::getUserStatistics(const std::string name)
{
    std::vector<std::string> player_state;
    player_state.push_back(name);
    player_state.push_back(to_string(m_database->getAveargeAnswerTime(name)));
    player_state.push_back(to_string(m_database->getNumOfPlayerGames(name)));
    player_state.push_back(to_string(m_database->getNumOfCorrectAnswers(name)));
    player_state.push_back(to_string(m_database->getNumOfTotalAnswers(name)));
    return player_state;
}

int StatisticsManager::calcScore(const string name)
{
    int score = 0;
    int correct = 0;
    std::vector<std::string> temp = getUserStatistics(name);
    std::vector<std::string>::iterator it = temp.begin();
    it += 3;
    correct = atoi(it->data());
    score += correct * 100;
    it++;
    score -= (atoi(it->data()) - correct) * 10;
    return score;
    
}
