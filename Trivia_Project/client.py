import socket
import json

SERVER = "127.0.0.1"
PORT = 8826

LOGIN_RESPONSE = "1"
SIGN_UP_RESPONSE = "2"
ERROR_RESPONSE = "3"
SIGN_OUT_RESPONSE = "4"
GET_ROOM_RESPONSE = "5"
GET_HIGH_SCORE_RESPONSE = "6"
GET_PERSON_STATS_RESPONSE = "7"
JOIN_ROOM_RESPONSE = "8"
CREATE_ROOM_RESPONSE = "9"
GET_PLAYERS_IN_ROOM = "10"


def client():
    login_req = {"username": "user3h", "password": "1234"}
    signup_req = {"username": "user3h", "password": "1234", "mail": "user1@gmail.com"}
    empty = {}    # logout, getRooms, getHighScore, getPersonalStats

    data_login_handler = json.dumps(signup_req)
    data_menu_handler = json.dumps(empty)

    len_login_msg = "00" + str(len(data_login_handler))
    len_menu_msg = "00" + str(len(data_menu_handler))

    msg_login_handler = bytearray(LOGIN_RESPONSE, "ascii") + bytearray(len_login_msg, "ascii") + bytearray(data_login_handler, "ascii")    # 41(decimal) = 29(hex)
    msg_menu_handler = bytearray(SIGN_OUT_RESPONSE, "ascii") + bytearray(len_menu_msg, "ascii") + bytearray(data_menu_handler, "ascii")    # 41(decimal) = 29(hex)

    connected = False
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER, PORT)
    try:
        sock.connect(server_address)
        connected = True
    except Exception:
        print("no server found!")

    while True:
        sock.sendall(msg_login_handler)
        print(sock.recv(1024).decode())

        sock.sendall(msg_menu_handler)
        print(sock.recv(1024).decode())

    # sock.close()


def main():
    client()


if __name__ == '__main__':
    main()
